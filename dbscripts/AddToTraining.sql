USE [Employee_Management]
GO

INSERT INTO [dbo].[TrainingFiles]
           ([TrainingId]
           ,[OrderNo]
           ,[Description]
           ,[FilePath]
           ,[IsComposite]
           ,[TrainingIdReference]
           ,[OrderNoReference])
     VALUES
           (<TrainingId, int,>
           ,<OrderNo, int,>
           ,<Description, varchar(200),>
           ,<FilePath, varchar(100),>
           ,<IsComposite, bit,>
           ,<TrainingIdReference, varchar(100),>
           ,<OrderNoReference, varchar(100),>)
GO


