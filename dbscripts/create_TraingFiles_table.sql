USE [Employee_Management]
GO

/****** Object:  Table [dbo].[TrainingFiles]    Script Date: 7/5/2020 1:15:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TrainingFiles](
	[TrainingId] [int] NOT NULL,
	[OrderNo] [int] NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[FilePath] [varchar](100) NULL,
	[IsComposite] [bit] NOT NULL,
	[TrainingIdReference] [varchar](100) NULL,
	[OrderNoReference] [varchar](100) NULL,
 CONSTRAINT [PK_TrainingFiles] PRIMARY KEY CLUSTERED 
(
	[TrainingId] ASC,
	[OrderNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


