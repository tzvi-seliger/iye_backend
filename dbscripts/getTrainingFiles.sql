

--make output table and for each record 
--if it is not composite add it
--if it is composite and ordernoref <> null add the ref for the traininfref/ordernoref
--if its composite and there is no orderno add all files for trainingIdRef this may be nested multiple times


  --if idref is null make idref id and ordernoref orderno
  select [TrainingId]
      ,[OrderNo]
      ,[Description]
      ,[FilePath]
      ,[IsComposite]
      ,trainingid as [TrainingIdReference]
      ,orderno as [OrderNoReference] from trainingfiles
	  where iscomposite  = 0
	  union
	  --return where idref is not null and ordernoref is null all trainings for id ref
	  select 
	   a.[TrainingId]
      ,a.[OrderNo]
      ,a.[Description]
      ,b.[FilePath]
      ,b.[IsComposite]
      ,b.trainingid as [TrainingIdReference]
      ,b.orderno as [OrderNoReference] from trainingfiles a
	  join trainingfiles b on a.trainingIdReference = b.trainingId
	 where a.iscomposite = 1 and a.ordernoreference is null
	 union
	  select 
	   a.[TrainingId]
      ,a.[OrderNo]
      ,a.[Description]
      ,b.[FilePath]
      ,a.[IsComposite]
      ,b.trainingid as [TrainingIdReference]
      ,b.orderno as [OrderNoReference] from trainingfiles a
	  join trainingfiles b on a.trainingIdReference = b.trainingId
	 where a.iscomposite = 1 and a.ordernoreference is not null and a.ordernoreference = b.orderno