USE [master]
GO
/****** Object:  Database [Employee_Management]    Script Date: 2/19/2023 10:48:23 PM ******/
CREATE DATABASE [Employee_Management]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Employee_Management', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Employee_Management.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Employee_Management_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Employee_Management_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Employee_Management] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Employee_Management].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Employee_Management] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Employee_Management] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Employee_Management] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Employee_Management] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Employee_Management] SET ARITHABORT OFF 
GO
ALTER DATABASE [Employee_Management] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Employee_Management] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Employee_Management] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Employee_Management] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Employee_Management] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Employee_Management] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Employee_Management] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Employee_Management] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Employee_Management] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Employee_Management] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Employee_Management] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Employee_Management] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Employee_Management] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Employee_Management] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Employee_Management] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Employee_Management] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Employee_Management] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Employee_Management] SET RECOVERY FULL 
GO
ALTER DATABASE [Employee_Management] SET  MULTI_USER 
GO
ALTER DATABASE [Employee_Management] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Employee_Management] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Employee_Management] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Employee_Management] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Employee_Management] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Employee_Management', N'ON'
GO
ALTER DATABASE [Employee_Management] SET QUERY_STORE = OFF
GO
USE [Employee_Management]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[AccountID] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](50) NOT NULL,
	[AccountDescription] [varchar](1000) NOT NULL,
	[AccountLogo] [varchar](100) NULL,
 CONSTRAINT [pk_AccountID] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AlbumItems]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlbumItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PortfolioId] [int] NOT NULL,
	[PhotoPath] [varchar](1000) NOT NULL,
 CONSTRAINT [pk_AlbumItemsId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Articles]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Articles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[Content] [varchar](max) NULL,
	[Rating] [int] NULL,
	[Author] [varchar](50) NULL,
	[Level] [varchar](20) NULL,
	[Subject] [varchar](50) NULL,
 CONSTRAINT [PK_ArticleId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Benefits]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Benefits](
	[BenefitId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Length] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[Limit] [int] NOT NULL,
	[StartOffset] [int] NOT NULL,
 CONSTRAINT [pk_BenefitId] PRIMARY KEY CLUSTERED 
(
	[BenefitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CallHistoryItems]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CallHistoryItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CallingPartyId] [int] NOT NULL,
	[CallTimeStamp] [datetime] NOT NULL,
	[RateAtTimeOfCall] [int] NOT NULL,
	[EndCallTimeStamp] [datetime] NULL,
 CONSTRAINT [pk_CallHistoryItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](20) NOT NULL,
	[ArticleId] [varchar](20) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Content] [varchar](200) NULL,
 CONSTRAINT [PK_CommentId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CompletedTrainings]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompletedTrainings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TrainingId] [int] NOT NULL,
 CONSTRAINT [PK_CompletedTrainingsId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Interests]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Interests](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Interest] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobTitles]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobTitles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[JobTitle] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Links]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Links](
	[LinkId] [int] IDENTITY(1,1) NOT NULL,
	[Link] [varchar](50) NOT NULL,
	[LinkDescription] [varchar](1000) NOT NULL,
	[AccountLogo] [varchar](100) NULL,
 CONSTRAINT [pk_LinkId] PRIMARY KEY CLUSTERED 
(
	[LinkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Messages]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SenderId] [varchar](20) NOT NULL,
	[ReceiverId] [varchar](20) NOT NULL,
	[Content] [varchar](200) NULL,
 CONSTRAINT [PK_MessageId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PortfolioItems]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PortfolioItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Title] [varchar](1000) NOT NULL,
	[Detail] [varchar](100) NULL,
	[SpanStart] [datetime] NOT NULL,
	[SpanEnd] [datetime] NOT NULL,
 CONSTRAINT [pk_PortfolioItemsId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PositionBenefits]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionBenefits](
	[PositionBenefitId] [int] IDENTITY(1,1) NOT NULL,
	[PositionId] [int] NOT NULL,
	[BenefitId] [int] NOT NULL,
 CONSTRAINT [pk_PositionBenefitId] PRIMARY KEY CLUSTERED 
(
	[PositionBenefitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PositionExperienceRequirements]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionExperienceRequirements](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PositionId] [int] NOT NULL,
	[JobTitleId] [int] NOT NULL,
	[ExperienceMinimum] [int] NOT NULL,
	[ExperienceMaximum] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PositionInterestsRequirements]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionInterestsRequirements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PositionId] [int] NOT NULL,
	[InterestId] [int] NULL,
	[InterestTitle] [varchar](100) NULL,
	[InterestDescription] [varchar](300) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Positions](
	[PositionId] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[SalaryMin] [int] NOT NULL,
	[SalaryMax] [int] NOT NULL,
	[Location] [varchar](10) NOT NULL,
	[IsRemote] [bit] NOT NULL,
	[ExperienceRequired] [int] NOT NULL,
	[Title] [varchar](100) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [pk_PositionId] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PositionSkillsRequirements]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionSkillsRequirements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PositionId] [int] NOT NULL,
	[SkillId] [int] NULL,
	[SkillTitle] [varchar](100) NULL,
	[SkillDescription] [varchar](300) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rates]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Rate] [int] NOT NULL,
 CONSTRAINT [pk_RatesId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Skills]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Skills](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Skill] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subjects]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [varchar](50) NOT NULL,
 CONSTRAINT [pk_SubjectId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrainingFiles]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingFiles](
	[TrainingId] [int] NOT NULL,
	[OrderNo] [int] NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[FilePath] [varchar](100) NULL,
	[IsComposite] [bit] NOT NULL,
	[TrainingIdReference] [varchar](100) NULL,
	[OrderNoReference] [varchar](100) NULL,
 CONSTRAINT [PK_TrainingFiles] PRIMARY KEY CLUSTERED 
(
	[TrainingId] ASC,
	[OrderNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trainings]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trainings](
	[TrainingID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[TrainingName] [varchar](50) NULL,
	[TrainingDescription] [varchar](200) NULL,
	[UserId] [int] NULL,
 CONSTRAINT [pk_trainingID] PRIMARY KEY CLUSTERED 
(
	[TrainingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserCompletedTrainings]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCompletedTrainings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TrainingId] [int] NOT NULL,
 CONSTRAINT [pk_UserCompletedTrainingsId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserInterests]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInterests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Interest] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[UserType] [varchar](50) NOT NULL,
	[UserName] [varchar](50) NULL,
	[PasswordString] [varchar](50) NULL,
	[Salt] [varchar](50) NULL,
	[UserEmailAddress] [varchar](50) NOT NULL,
	[UserFirstName] [varchar](50) NOT NULL,
	[UserLastName] [varchar](50) NOT NULL,
	[UserPhoneNumber] [varchar](15) NOT NULL,
	[ManagerId] [int] NULL,
	[ProfilePhotoFilepath] [varchar](100) NULL,
 CONSTRAINT [pk_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSkills]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSkills](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[Skill] [varchar](50) NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTrainings]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainings](
	[UserTrainingID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[TrainingID] [int] NOT NULL,
	[TrainingStatus] [varchar](20) NULL,
	[IsComplete] [bit] NULL,
 CONSTRAINT [PK_UserTrainingID] PRIMARY KEY CLUSTERED 
(
	[UserTrainingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserWorkExperiences]    Script Date: 2/19/2023 10:48:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserWorkExperiences](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[UserId] [int] NOT NULL,
	[WorkTitle] [varchar](100) NOT NULL,
	[WorkDescription] [varchar](200) NOT NULL,
	[WorkStartDate] [date] NOT NULL,
	[WorkEndDate] [date] NOT NULL,
 CONSTRAINT [pk_ID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Accounts] ON 

INSERT [dbo].[Accounts] ([AccountID], [AccountName], [AccountDescription], [AccountLogo]) VALUES (1, N'Amazon', N'Retail Online Marketplace', N'FileName/AmazonLogo')
INSERT [dbo].[Accounts] ([AccountID], [AccountName], [AccountDescription], [AccountLogo]) VALUES (2, N'Apple', N'Electronics Manufacturer', N'FileName/AppleLogo')
INSERT [dbo].[Accounts] ([AccountID], [AccountName], [AccountDescription], [AccountLogo]) VALUES (3, N'Google', N'Search Online', N'FileName/GoogleLogo')
INSERT [dbo].[Accounts] ([AccountID], [AccountName], [AccountDescription], [AccountLogo]) VALUES (5, N'Softwriters', N'Long Term Care Pharmacy Software', N'FileName/SoftwritersLogo')
SET IDENTITY_INSERT [dbo].[Accounts] OFF
SET IDENTITY_INSERT [dbo].[AlbumItems] ON 

INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (1, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (2, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (3, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (4, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (5, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (6, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (7, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (8, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (9, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (10, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (11, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (12, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (13, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (14, 1, N'http://photo.path')
INSERT [dbo].[AlbumItems] ([Id], [PortfolioId], [PhotoPath]) VALUES (15, 1, N'http://photo.path')
SET IDENTITY_INSERT [dbo].[AlbumItems] OFF
SET IDENTITY_INSERT [dbo].[Articles] ON 

INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (8, N'Javascript - a horrible language', N'a backround on the idiosyncracies of Javascript', N'Sample Content - to be edited', 1, N'Tzvi Seliger', N'Beginner', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (9, N'Design Patterns', N'Design patterns and their application', N'Sample Content - to be edited', 4, N'Anthony Tedesco', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (10, N'C# Language Features', N'c# language features detailed with practical usage examples', N'Sample Content - to be edited', 3, N'Scott Sinclair', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (11, N'general data structures', N'general data structures detailed with practical usage examples', N'Sample Content - to be edited', 5, N'Nathan Wrighgt', N'Intermediate', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (12, N'algorithms', N'algorithms detailed with practical usage examples', N'Sample Content - to be edited', 4, N'Judson Weissert', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (13, N'writing APIs', N'writing APIs with GO, python', N'Sample Content - to be edited', 4, N'Robert Smith', N'Intermediate', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (14, N'optical recognition', N'optical recognition', N'Sample Content - to be edited', 4, N'Josh Zinkovsky', N'Intermediate', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (15, N'audio engineering', N'audio engineering', N'Sample Content - to be edited', 4, N'Josh Zinkovsky', N'Intermediate', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (16, N'training machine learning models', N'training machine learning models', N'Sample Content - to be edited', 4, N'Zachary Archer', N'Intermediate', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (17, N'programming iot devices', N'programming iot devices', N'Sample Content - to be edited', 4, N'Tzvi Seliger', N'Beginner', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (18, N'pipelines /cicd', N'pipelines /cicd', N'Sample Content - to be edited', 4, N'Stan Stoyko', N'Beginner', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (19, N'full software testing ', N'full software testing ', N'Sample Content - to be edited', 4, N'Reid Campolong', N'Beginner', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (20, N'agile scrum methodology  ', N'agile scrum methodology  ', N'Sample Content - to be edited', 4, N'Justin Kramer', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (21, N'hardware testing', N'hardware testing', N'Sample Content - to be edited', 4, N'Bala Mageswaran', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (22, N'relevant mathematics for algorithms ', N'relevant mathematics for algorithms ', N'Sample Content - to be edited', 4, N'Jena Gutshall', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (23, N'relevant mathematics for machine learning  ', N'relevant mathematics for machine learning  ', N'Sample Content - to be edited', 4, N'Norman Caves', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (24, N'implementation of UI /UX principles', N'implementation of UI /UX principles', N'Sample Content - to be edited', 4, N'Anthony Wilson', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (25, N'reliability engineering', N'reliability engineering', N'Sample Content - to be edited', 4, N'Yossi Itzinger', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (26, N'software architecture ', N'software architecture ', N'Sample Content - to be edited', 4, N'Heshy Hoffinger', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (27, N'building microservices', N'building microservices', N'Sample Content - to be edited', 4, N'Heshy Hoffinger', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (28, N'database administration', N'database administration', N'Sample Content - to be edited', 4, N'Raizel Seliger', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (29, N'game graphics', N'game graphics', N'Sample Content - to be edited', 4, N'Cameron Asbury', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (30, N'networks', N'networks', N'Sample Content - to be edited', 4, N'Eli Wasserman', N'Advanced', N'Computer Science')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (35, N'Basic Algebra', N'Basic Algebra Lesson', N'Teach you how to find the X ', 3, N'', N'Beginner', N'Tzvi Seliger')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (36, N'Git Basics', N'Git to Know because its git to know', N'All the details of git', 4, N'', N'Advanced', N'Tzvi Seliger')
INSERT [dbo].[Articles] ([Id], [Title], [Description], [Content], [Rating], [Author], [Level], [Subject]) VALUES (37, N'???? ?????', N'public domain', N'This is the public domain specifically as it relates to the laws of Shabbos in jewish tradition', 3, N'', N'beginner', N'Tzvi Seliger')
SET IDENTITY_INSERT [dbo].[Articles] OFF
SET IDENTITY_INSERT [dbo].[CallHistoryItems] ON 

INSERT [dbo].[CallHistoryItems] ([Id], [UserId], [CallingPartyId], [CallTimeStamp], [RateAtTimeOfCall], [EndCallTimeStamp]) VALUES (1, 1, 2, CAST(N'2022-08-21T12:30:43.883' AS DateTime), 25, CAST(N'2022-08-21T23:30:43.883' AS DateTime))
SET IDENTITY_INSERT [dbo].[CallHistoryItems] OFF
SET IDENTITY_INSERT [dbo].[PortfolioItems] ON 

INSERT [dbo].[PortfolioItems] ([ID], [UserId], [Title], [Detail], [SpanStart], [SpanEnd]) VALUES (1, 1, N'Personal Website', N'A website that is used for a personal portfolio website', CAST(N'2022-02-12T02:32:55.000' AS DateTime), CAST(N'2022-05-24T02:32:55.000' AS DateTime))
INSERT [dbo].[PortfolioItems] ([ID], [UserId], [Title], [Detail], [SpanStart], [SpanEnd]) VALUES (2, 1, N'Employee Management Website', N'Give your people opportunity to grow exponentially', CAST(N'2022-05-24T02:32:55.000' AS DateTime), CAST(N'2022-12-24T02:32:55.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[PortfolioItems] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (4, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (5, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (6, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (7, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (8, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (9, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (10, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (11, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
INSERT [dbo].[Positions] ([PositionId], [AccountID], [SalaryMin], [SalaryMax], [Location], [IsRemote], [ExperienceRequired], [Title], [Description]) VALUES (12, 1, 25234, 40000, N'1', 1, 5, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Positions] OFF
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1, 6, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1, 7, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1, 8, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1, 10, N'My New File', N'myfilepath', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (2, 10, N'My New File', N'myfilepath', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 10, N'My New File', N'myfilepath', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 11, N'My New File', N'myfilepath', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 12, N'My New File', N'myfilepath', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 13, N'My New File', N'james.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 14, N'My New File', N'james.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 15, N'My New File', N'james.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 16, N'My New File', N'james.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 17, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 18, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (82, 19, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (84, 1, N'HTML Programming', NULL, 1, N'88', N'1')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (84, 2, N'HTML Attributes', NULL, 1, N'89', NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (84, 3, N'HTML Nest', NULL, 1, N'90', NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (88, 1, N'Tags Synopsis', N'tags.path', 0, NULL, NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (88, 2, N'HTML Tags 2', N'tags2.path', 0, NULL, NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (89, 1, N'Attributes Synopsis', N'attributes.path', 0, NULL, NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (89, 2, N'HTML attributes2', N'attributes2.path', 0, NULL, NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (90, 1, N'HTML Nest', N'nest.path', 0, NULL, NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (90, 2, N'HTML Nest2', N'nest2.path', 0, NULL, NULL)
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (99, 19, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (100, 22, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (103, 20, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (103, 21, N'My New File', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (103, 35, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (103, 36, N'File Description', N'capture.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 1, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 2, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 3, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 4, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 5, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 6, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 7, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 8, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 9, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 10, N'Chemical Terms 1', N'chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 11, N'Chemical Terms 1', N'chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 12, N'Chemical Terms 1', N'chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 13, N'Chemical Terms 1', N'chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 14, N'Chemical Terms 1', N'chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 15, N'Chemical Terms 1', N'chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 17, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\chemterms.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 18, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\headshot.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 19, N'Chemical Terms 1', N'C:\Users\Raizel Seliger\Desktop\headshot.jpg', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 25, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (105, 26, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 1, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 2, N'File Description', N'Path.Path', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 3, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 4, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 5, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 6, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 7, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 8, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 9, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1112, 10, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
INSERT [dbo].[TrainingFiles] ([TrainingId], [OrderNo], [Description], [FilePath], [IsComposite], [TrainingIdReference], [OrderNoReference]) VALUES (1113, 11, N'File Description', N'C:\Users\Raizel Seliger\Pictures\Screenshots\School-Districts-Toggle.png', 0, N'0', N'0')
SET IDENTITY_INSERT [dbo].[Trainings] ON 

INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (82, 1, N'Programming', N'Become a programmer in 365 days', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (83, 1, N'learn Math in 7 days', N'Become a Mathematician in 7 days', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (84, 1, N'learn HTML in 2 days', N'Become an HTML Master in 2 days', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (85, 1, N'learn CSS in 7 days', N'Become CSS master in 7 days', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (86, 1, N'learn Javascript in 2 Weeks', N'Become a Javascript novice in 2 weeks', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (87, 1, N'learn SQL in 1 week', N'Become DB novice in 7 days', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (88, 1, N'HTML Tags', N'Tags - What are they? - What the heck is markup?', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (89, 1, N'HTML Attributes', N'Head Scratch - attributes? -getting philosophical', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (90, 1, N'HTML Nest', N'Because HTML Tags get lonely', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (91, 1, N'Why do we need HTML', N'HTML Practically', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (92, 1, N'Programming in a nutshell', N'What is it', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (93, 1, N'Programming in a (larger) nutshell', N'more detail', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (94, 1, N'Know IT in 180 days', N'Become an IT master in 180 days', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (95, 1, N'IT and programming - whats the difference', N'Learn the Difference between IT and programming', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (96, 1, N'IT in a nutshell', N'I know nuthin', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (97, 1, N'Html Introduction', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (98, 1, N'Html Introduction', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (99, 1, N'Html Introduction', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (100, 1, N'Html Intermediate', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (101, 1, N'Html Advanced', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (102, 1, N'Javascript', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (103, 1, N'Javascript', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (104, 1, N'yuffiyu', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (105, 1, N'Chemical Engineering Terms', N'Chemical Engineering Terms so youre not like a deer in the headlights', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (106, 1, N'Html Introduction', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1106, 1, N'Html Introduction', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1107, 1, N'Html Introduction', N'html for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1108, 1, N'java', N'java for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1109, 1, N'java', N'java for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1110, 1, N'java', N'java for dummies', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1111, 1, N'test new', N'test new desc', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1112, 1, N'test new', N'test new desc', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1113, 1, N'testnew', N'test new desc', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1114, 1, N'testnew', N'test new desc', 7)
INSERT [dbo].[Trainings] ([TrainingID], [AccountID], [TrainingName], [TrainingDescription], [UserId]) VALUES (1115, 1, N'testnew', N'test new desc', 7)
SET IDENTITY_INSERT [dbo].[Trainings] OFF
SET IDENTITY_INSERT [dbo].[UserInterests] ON 

INSERT [dbo].[UserInterests] ([Id], [UserID], [Interest], [Description]) VALUES (2, 1, N'Biology', N'The study of living things')
INSERT [dbo].[UserInterests] ([Id], [UserID], [Interest], [Description]) VALUES (3, 1, N'Biology', N'The study of living things')
INSERT [dbo].[UserInterests] ([Id], [UserID], [Interest], [Description]) VALUES (1002, 1, N'Computer Science', N'The study of computers')
SET IDENTITY_INSERT [dbo].[UserInterests] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (1, 1, N'User', N'Fatso', N'password', N'salt', N'michaeljones@amazon.com', N'Michael', N'jones', N'7712221111', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (2, 1, N'User', N'fatso2', N'password', N'salt', N'brianjones@amazon.com', N'brian', N'jones', N'7712221112', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (3, 2, N'User', N'skinnyso', N'password', N'salt', N'mikebrand@apple.com', N'mike', N'brand', N'7712221133', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (4, 2, N'User', N'skinnyso2', N'password', N'salt', N'nancyhanson@apple.com', N'nancy', N'hanson', N'7712221141', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (6, 1, N'User', N'user1', N'password1', N'salt1', N'user1@gmail.com', N'Mike', N'test', N'111-222-3333', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (7, 1, N'User', N'user789', N'password', N'saltstring', N'fatso212@gmail.com', N'Tzvi', N'Seliger', N'602-710-5010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (8, 1, N'User', N'user790', N'password', N'saltstring', N'fatso212@gmail.com', N'Tzvi', N'Seliger', N'602-710-5010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (9, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (10, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (11, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (12, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (13, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (14, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (15, 1, N'User', N'user100', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (16, 1, N'User', N'user1012344', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (17, 1, N'User', N'user1012344', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (18, 1, N'User', N'user1012344', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (19, 1, N'User', N'user1012344', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (20, 1, N'User', N'user1012344', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (21, 1, N'User', N'user1012344', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (22, 1, N'User', N'user1012234234', N'password', N'salt', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (23, 1, N'user', N'user234', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (24, 1, N'user', N'user345', N'password', N'passsword', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (25, 1, N'USER', N'USER3405', N'PASSWORD', N'PASSWORD', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (26, 1, N'user', N'usser4359034', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (27, 1, N'user', N'user340', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (28, 1, N'user', N'user103', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (29, 1, N'user', N'user103', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (30, 1, N'user', N'user234', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (31, 1, N'user', N'user346', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (32, 1, N'user', N'user46', N'passwowrd', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (33, 1, N'user', N'user678', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (34, 1, N'user', N'user678', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (35, 1, N'user', N'user4567', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (36, 1, N'user', N'user765', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (37, 1, N'user', N'user0224', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (38, 1, N'user', N'user4-=34', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (39, 1, N'user', N'user-=34', N'password', N'password', N'fatso212@gmail.com', N'tzvi', N'seliger', N'16027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (40, 1, N'user', N'USER056=', N'password', N'password', N'tzviseliger@gmail.com', N'Tzvi', N'Seliger', N'6027105010', NULL, NULL)
INSERT [dbo].[Users] ([UserID], [AccountID], [UserType], [UserName], [PasswordString], [Salt], [UserEmailAddress], [UserFirstName], [UserLastName], [UserPhoneNumber], [ManagerId], [ProfilePhotoFilepath]) VALUES (42, 3, N'User', N'Tony360', N'tonyuierg', N'2345', N'tony@ymail.com', N'Anthony', N'Tedesco', N'123-4567', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[UserSkills] ON 

INSERT [dbo].[UserSkills] ([Id], [UserID], [Skill], [Description]) VALUES (2, 1, N'Computer Science', N'The study of computers')
INSERT [dbo].[UserSkills] ([Id], [UserID], [Skill], [Description]) VALUES (1002, 1, N'Computer Science', N'The study of computers')
INSERT [dbo].[UserSkills] ([Id], [UserID], [Skill], [Description]) VALUES (2002, 1, N'Computer Science', N'The study of computers')
SET IDENTITY_INSERT [dbo].[UserSkills] OFF
SET IDENTITY_INSERT [dbo].[UserTrainings] ON 

INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (5, 7, 105, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (6, 1, 82, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (7, 7, 90, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (8, 7, 90, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (9, 7, 90, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (10, 1, 97, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (11, 1, 97, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (12, 1, 98, N'Recommended', 1)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (13, 7, 105, N'Recommended', NULL)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (1013, 23, 1112, N'Recommended', NULL)
INSERT [dbo].[UserTrainings] ([UserTrainingID], [UserID], [TrainingID], [TrainingStatus], [IsComplete]) VALUES (1014, 23, 1112, N'Recommended', NULL)
SET IDENTITY_INSERT [dbo].[UserTrainings] OFF
SET IDENTITY_INSERT [dbo].[UserWorkExperiences] ON 

INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (1, 1, 2, N'Construction Foreman', N'managing construction', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (2, 1, 2, N'software manager', N'managing construction', CAST(N'2012-12-24' AS Date), CAST(N'2016-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (3, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (4, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (5, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (6, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (7, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (8, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (9, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (10, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (11, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (12, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (13, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (14, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (15, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (16, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (17, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (18, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (19, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (20, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (21, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (22, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (23, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (24, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (25, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (26, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (27, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (28, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (29, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (30, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (31, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (32, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (33, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (34, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (35, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (36, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (37, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (38, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (39, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (40, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (41, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (42, 1, 2, N'test', N'new', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (43, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (44, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (45, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (46, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (47, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (48, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (49, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (1045, 1, 2, N'Lab Intern', N'Conducted experiments on the effects of chemicals coming in contact', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (2046, 1, 2, N'Construction Foreman', N'managing construction', CAST(N'1978-12-20' AS Date), CAST(N'1979-12-20' AS Date))
INSERT [dbo].[UserWorkExperiences] ([Id], [AccountID], [UserId], [WorkTitle], [WorkDescription], [WorkStartDate], [WorkEndDate]) VALUES (2047, 1, 2, N'Civil Engineer', N'Building Bridges', CAST(N'1978-12-20' AS Date), CAST(N'2012-12-23' AS Date))
SET IDENTITY_INSERT [dbo].[UserWorkExperiences] OFF
ALTER TABLE [dbo].[Trainings]  WITH CHECK ADD FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([AccountID])
REFERENCES [dbo].[Accounts] ([AccountID])
GO
ALTER TABLE [dbo].[UserTrainings]  WITH CHECK ADD FOREIGN KEY([TrainingID])
REFERENCES [dbo].[Trainings] ([TrainingID])
GO
ALTER TABLE [dbo].[UserTrainings]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [CHK_Salt] CHECK  (([UserType]<>'User' OR [Salt] IS NOT NULL))
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [CHK_Salt]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [CHK_User] CHECK  (([UserType]='Employee' OR [UserType]='User' OR [UserType]='Admin'))
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [CHK_User]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [CHK_UserName] CHECK  (([UserName] IS NOT NULL OR [UserType]<>'User'))
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [CHK_UserName]
GO
ALTER TABLE [dbo].[UserTrainings]  WITH CHECK ADD  CONSTRAINT [CHK_TrainingStatus] CHECK  (([TrainingStatus]='Recommended' OR [TrainingStatus]='Requested' OR [TrainingStatus]='In Progress' OR [TrainingStatus]='Completed'))
GO
ALTER TABLE [dbo].[UserTrainings] CHECK CONSTRAINT [CHK_TrainingStatus]
GO
/****** Object:  StoredProcedure [dbo].[GetRecords]    Script Date: 2/19/2023 10:48:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create     PROCEDURE [dbo].[GetRecords] (@MyOutput varchar(100) OUTPUT)
AS
SELECT @MyOutput = 'Hello'
GO
/****** Object:  StoredProcedure [dbo].[SelectUsers]    Script Date: 2/19/2023 10:48:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SelectUsers]

AS
Begin;
select * from dbo.users
END;
GO
USE [master]
GO
ALTER DATABASE [Employee_Management] SET  READ_WRITE 
GO
