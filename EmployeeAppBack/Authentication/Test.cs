﻿using Microsoft.AspNetCore.Authentication;
using System;

namespace EmployeeAppBack.Authentication
{
    public class Test
    {
        public AuthenticationSchemeBuilder _authenticationSchemeBuilder;
        public Type HandlerType { get; set; }

        public string DisplayName { get; set; }
    }
}