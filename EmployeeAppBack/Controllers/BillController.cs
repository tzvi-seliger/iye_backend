﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.BillHistory;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class BillController : Controller
    {

        AddCallHistoryItem addCallHistoryItem;
        DeleteCallHistoryItem deleteCallHistoryItem;
        EditCallHistoryItem editCallHistoryItem;
        GetBillTotal getBillTotal;
        GetCallHistoryItems getCallHistoryItems;

        public BillController(
            AddCallHistoryItem _addCallHistoryItem,
            DeleteCallHistoryItem _deleteCallHistoryItem,
            EditCallHistoryItem _editCallHistoryItem,
            GetBillTotal _getBillTotal,
            GetCallHistoryItems _getCallHistoryItems
         )
        {
            addCallHistoryItem = _addCallHistoryItem;
            deleteCallHistoryItem = _deleteCallHistoryItem;
            editCallHistoryItem = _editCallHistoryItem;
            getBillTotal = _getBillTotal;
            getCallHistoryItems = _getCallHistoryItems;
        }

        [HttpGet]
        public IActionResult GetBillHistory(string id)
        {
            List<BillHistoryEntry> positions = getCallHistoryItems.Execute(id);
            return Ok(positions);
        }

        [HttpPut]
        public IActionResult AddToBillHistory([FromBody]BillHistoryEntry billHistoryEntry)
        {
            addCallHistoryItem.Execute(billHistoryEntry);
            return Ok();
        }

        [HttpPost]
        public IActionResult EditBillHistory([FromBody]BillHistoryEntry billHistoryEntry)
        {
            editCallHistoryItem.Execute( billHistoryEntry);
            return Ok();
        }

        [HttpDelete]
        [Route("history/{id}")]
        public IActionResult DeleteFromBillHistory(string id)
        {
            deleteCallHistoryItem.Execute(id);
            return Ok();
        }

        [HttpGet]
        [Route("total/{id}")]
        public IActionResult GetTotal(string id)
        {
            int total = getBillTotal.Execute(id);
            return Ok(total);
        }
    }
}
