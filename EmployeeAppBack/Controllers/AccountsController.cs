﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.Accounts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using EmployeeAppBack.Services;
using Microsoft.AspNetCore.Cors;

namespace EmployeeAppBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]

    public class AccountsController : ControllerBase
    {
        Accounts _accountsService;

        public AccountsController (
            GetAccounts getAccounts,
            GetAccountById getAccountById,
            AddAccount addAccount,
            DeleteAccountById deleteAccountById,
            UpdateAccountById updateAccountById
        )
        {
            _accountsService = new Accounts(
                getAccounts, 
                getAccountById, 
                addAccount, 
                deleteAccountById, 
                updateAccountById
            );
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_accountsService.GetAccounts());
        }

        [HttpGet]
        [Route("{accountId}")]
        public IActionResult Get(int accountId)
        {
            return Ok(_accountsService.GetAccountById(accountId));
        }

        [HttpPost]
        public IActionResult AddAccount(Account account)
        {
            _accountsService.AddAccount(account);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteAccountById(int accountId)
        {
            _accountsService.DeleteAccountById(accountId);
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateAccountById(Account account)
        {
            _accountsService.UpdateAccountById(account);
            return Ok();
        }

    }
}