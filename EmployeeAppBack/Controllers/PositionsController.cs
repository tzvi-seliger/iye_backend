﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.Positions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EmployeeAppBack.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class PositionsController : ControllerBase
    {
        EditPosition editPosition;
        AddPosition addPosition;
        DeletePosition deletePosition;
        GetPositions getPositions;


        public PositionsController(
            EditPosition _editPosition,
            AddPosition _addPosition,
            GetPositions _getPositions,
            DeletePosition _deletePosition
         )
        {
            editPosition = _editPosition;
            addPosition = _addPosition;
            deletePosition = _deletePosition;
            getPositions = _getPositions;
        }

        [HttpGet]
        [Route("{userId}")]
        public IActionResult GetPositions(int userId)
        {
            List<Position> positions = getPositions.Execute(userId);
            return Ok(positions);
        }

        [HttpPost]
        public IActionResult AddPosition([FromBody] Position position)
        {
            addPosition.Execute(position);
            return Ok();
        }

        [HttpPut]
        public IActionResult EditPosition([FromBody] Position position)
        {
            editPosition.Execute(position);
            return Ok();
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public IActionResult DeletePosition(string id)
        {
            deletePosition.Execute(id);
            return Ok();
        }
    }
}