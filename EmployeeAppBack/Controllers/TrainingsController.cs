﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries;
using EmployeeAppBack.Queries.Trainings;
using EmployeeAppBack.Queries.Users;
using EmployeeAppBack.Queries.Users.Trainings;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.IO.File;

namespace EmployeeAppBack.Controllers
{     
    [Route("api/trainings")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class TrainingsController : ControllerBase
    {
        // TODO Get AllFilesForTraining by training Id
        // TODO POST TrainingWithFiles
        // TODO PATCH UpdateTrainingAddNewFile
        // TODO PATCH UpdateTrainingAddExistingTraining
        // maybe just fill in the fields for all trainings even not composite
        // pretty much the same as it is now perhaps it would be slower

        private readonly Query _query;
        private readonly QueryReturn _queryReturn;
        private string AccountName { get; set; } = "Amazon";
        public GetTrainings getTrainings;
        public TrainingsByAccount trainingsByAccount;
        public GetEmployeeTrainingsByUserId _getEmployeeTrainingsByUserId;
        public AddUserTraining _addUserTraining;
        public GetEmployeeTrainingsById _getEmployeeTrainingsById;
        public GetLastTrainingNumber _getLastTrainingNumber;
        public GetTrainingById _getTrainingById;

        public TrainingsController(
            GetTrainings _getTrainings,
            TrainingsByAccount _trainingsByAccount,
            Query query,
            QueryReturn queryReturn,
            GetEmployeeTrainingsByUserId getEmployeeTrainingsByUserId,
            AddUserTraining addUserTraining,
            GetEmployeeTrainingsById getEmployeeTrainingsById,
            GetLastTrainingNumber getLastTrainingNumber,
            GetTrainingById getTrainingById

        )
        {
            getTrainings = _getTrainings;
            trainingsByAccount = _trainingsByAccount;
            _query = query;
            _queryReturn = queryReturn;
            _getEmployeeTrainingsByUserId = getEmployeeTrainingsByUserId;
            _addUserTraining = addUserTraining;
            _getEmployeeTrainingsById = getEmployeeTrainingsById;
            _getLastTrainingNumber = getLastTrainingNumber;
            _getTrainingById = getTrainingById;

    }

    [HttpGet]
        public IActionResult Get()
        {
            List<Training> trainings = getTrainings.Execute();
            return Ok(trainings);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            List<Training> trainings = trainingsByAccount.Execute(id);
            return Ok(trainings);
        }

        [HttpGet]
        [Route("GetEmployeeTrainings/{userName}")]
        public IActionResult GetEmployeeTrainings(string userName)
        {
            List<EmployeeTraining> trainings = _getEmployeeTrainingsByUserId.Execute(userName);
            return Ok(trainings);
        }

        [HttpGet]
        [Route("GetEmployeeTrainingsById/{userId}")]
        public IActionResult GetEmployeeTrainings(int userId)
        {
            List<EmployeeTraining> trainings = _getEmployeeTrainingsById.Execute(userId);
            return Ok(trainings);
        }

        [HttpPost]
        [Route("AddUserTraining")]
        public IActionResult AddUserTraining([FromBody] PostUserTraining employeeTraining)
        {
            _addUserTraining.Execute(employeeTraining);
            return Ok();
        }

        [HttpPost]
        public IActionResult Post([FromBody] Training training)
        {
   
            _query.Execute(new AddTraining(training));
            AddNewTraining(training.TrainingName, "Amazon");
            return Ok();
        }


        [HttpGet]
        [Route("files/{id}")]
        public IActionResult GetTrainingFiles(int id)
        {
            List<TrainingFile> files = _queryReturn.Execute(new GetTrainingFilesByTraining(id));
            foreach (var file in files)
            {
                file.FilePath = $@"C:\Users\Raizel Seliger\source\repos\emAppBack\EmployeeAppBack\AccountFolders\{file.AccountName}\Trainings\{file.TrainingName}\{file.FilePath.Split("\\").Last()}";
            }
            return Ok(files);
        }

        // need to
        [HttpPut]
        [Route("AddFile")]
        public IActionResult AddFileToTraining([FromBody] TrainingFileInsertion insertion)
        {
            try
            {
                _query.Execute(new AddFileToTraining(insertion));
                var trainingName = ReturnTrainings().Where(x => x.TrainingID == insertion.TrainingId).FirstOrDefault().TrainingName;
                AddFileToTraining(trainingName, insertion.FilePath, AccountName);
            }
            catch (Exception x)
            {
                throw x;
            }
            return Ok();
        }

        [HttpGet]
        [Route("CreateAccount/{pathName}")]
        public IActionResult CreateNewDirectory(string pathName)
        {
            string rootPath = $@"C:\Users\Seliger\source\repos\emAppBack\EmployeeAppBack\AccountFolders";
            string accountDirectory = Path.Combine(rootPath, pathName);
            Directory.CreateDirectory(accountDirectory);
            Directory.CreateDirectory(Path.Combine(accountDirectory, "Trainings"));

            return Ok();
        }

        [HttpGet]
        [Route("LastTrainingNumber")]
        public IActionResult GetLastTrainingNumber(int trainingId)
        {
            return Ok(_getLastTrainingNumber.Execute(_getTrainingById.Execute(trainingId).TrainingID));
        }

        [HttpGet]
        [Route("GetTraining/{id}")]
        public IActionResult GetTrainingById(int trainingId)
        {
            return Ok(_getTrainingById.Execute(trainingId));
        }

        public List<Training> ReturnTrainings()
        {
            List<Training> trainings = getTrainings.Execute();

            return trainings;
        }

        public void AddNewTraining([FromQuery] string pathName, [FromQuery] string account)
        {
            string rootPath = $@"C:\Users\Seliger\source\repos\iye_backend\EmployeeAppBack\AccountFolders";
            string accountDirectory = Path.Combine(rootPath, account);
            string TrainingsFolder = Path.Combine(accountDirectory, "Trainings");
            Directory.CreateDirectory(Path.Combine(TrainingsFolder, pathName));
        }

        public void AddFileToTraining(string trainingName, string sourcePath, string account)
        {
            string rootPath = $@"C:\Users\Seliger\source\repos\iye_backend\EmployeeAppBack\AccountFolders";
            string accountDirectory = Path.Combine(rootPath, account);
            string TrainingsFolder = Path.Combine(accountDirectory, "Trainings");
            var lastpath = sourcePath.Split("\\").Last();
            var destinationPath = Path.Combine(Path.Combine(TrainingsFolder, trainingName), lastpath);
            Directory.CreateDirectory(Path.Combine(TrainingsFolder, trainingName));
            Copy(sourcePath, destinationPath, true);
        }

        public void GetFilePath(string trainingName, string sourcePath, string account)
        {
            string rootPath = $@"C:\Users\Seliger\source\repos\iye_backend\EmployeeAppBack\AccountFolders";
            string accountDirectory = Path.Combine(rootPath, account);
            string TrainingsFolder = Path.Combine(accountDirectory, "Trainings");
            Copy(Path.Combine(@"C:\Users\Raizel Seliger\Pictures\Screenshots", sourcePath), Path.Combine(Path.Combine(TrainingsFolder, trainingName), sourcePath), true);
        }


    }
}

