﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.Users.Hierarchy;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;


namespace EmployeeAppBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class HierarchyController : ControllerBase
    {

        public GetRootManagerIdByAccountId getRootManagerIdByAccountId;
        public GetEmployeesByManagerId getEmployeesByManagerId;
        public GetAllManagerProfiles getAllManagerProfiles;
         public GetAllUsersInAccount getAllUsersInAccount;

        public HierarchyController (
            GetRootManagerIdByAccountId _getRootManagerIdByAccountId,
            GetEmployeesByManagerId _getEmployeesByManagerId,
            GetAllManagerProfiles _getAllManagerProfiles,
            GetAllUsersInAccount _getAllUsersInAccount

        )
        {
            getRootManagerIdByAccountId = _getRootManagerIdByAccountId;
            getEmployeesByManagerId  = _getEmployeesByManagerId;
            getAllManagerProfiles = _getAllManagerProfiles;
            getAllUsersInAccount = _getAllUsersInAccount;
        }

        [HttpGet]
        [Route("RootManager/{accountId}")]
        public IActionResult Get(int accountId)
        {
            List<UserHierarchy> users = getRootManagerIdByAccountId.Execute(accountId);
            return Ok(users);
        }

        [HttpGet]
        [Route("GetAllManagerProfiles/{accountId}")]
        public IActionResult GetAllManagerProfiles(int accountId)
        {
            List<UserHierarchy> users = getAllManagerProfiles.Execute(accountId);
            return Ok(users);
        }

        [HttpGet]
        [Route("GetEmployeesByManagerId/{accountId}/{managerId}")]
        public IActionResult GetEmployeesByManagerId(int accountId, int managerId)
        {
            List<UserHierarchy> users = getEmployeesByManagerId.Execute(accountId, managerId);
            return Ok(users);
        }

        [HttpGet]
        [Route("GetAllUsersInAccount/{accountId}")]
        public IActionResult GetAllUsersInAccount(int accountId)
        {
            List<UserHierarchy> users = getAllUsersInAccount.Execute(accountId);
            return Ok(users);
        }
    }
}