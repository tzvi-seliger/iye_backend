﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries;
using EmployeeAppBack.Queries.Users;
using EmployeeAppBack.Queries.Users.Interests;
using EmployeeAppBack.Queries.Users.Skills;
using EmployeeAppBack.Queries.Users.Trainings;
using EmployeeAppBack.Queries.Users.WorkExperience;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static System.IO.File;


namespace EmployeeAppBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class UsersController : ControllerBase
    {
        //todo employment history

        //get all users - done
        //get user by Id
        //get users by company
        //get users by skillset
        //get users by training - done
        //get users with multiple
        //get user skills
        //get user trainings -  this is in the trainings controller - should be switched
        //get user work experience
        //get user portfolio items
        //add item to portfolio
        //add item to work experience
        //add item to skills
        //add item to training - I think this already exists in the trainings controller - should probably be moved here
        //add item to interests
        //get work experience for a user
        //add work experience

        //users should have interests
        //users should have skills
        //users should have experience
        //users should have trainings

        //should be able to filter users by any combination of interests skills experience and training

        public GetUsers getUsers;
        public GetUserAuthoredTrainings getUserAuthoredTrainings;
        public GetUserCompletedTrainings getUserCompletedTrainings;
        public GetUsersByTraining getUsersByTraining;
        public GetUserByUserNameAndPassword getUserByUserNameAndPassword;
        public GetUserByUserId getUserByUserId;
        public AddUser addUser;
        public AddUserWorkExperience addUserWorkExperience;
        public GetUserWorkExperience getUserWorkExperience;
        public UpdateUserWorkExperience updateUserWorkExperience;
        public DeleteUserWorkExperience deleteUserWorkExperience;
        public GetUserInterests getUserInterests;
        public AddUserInterest addUserInterest;
        public UpdateUserInterest updateUserInterest;
        public DeleteUserInterest deleteUserInterest;
        public GetUserSkills getUserSkills;
        public AddUserSkill addUserSkill;
        public UpdateUserSkill updateUserSkill;
        public DeleteUserSkill deleteUserSkill;
        public UpdateUserInfo updateUserInfo;

        public UsersController(
            GetUsers _getUsers,
            GetUserAuthoredTrainings _getUserAuthoredTrainings,
            GetUserCompletedTrainings _getUserCompletedTrainings,
            GetUsersByTraining _getUsersByTraining,
            GetUserByUserNameAndPassword _getUserByUserNameAndPassword,
            GetUserByUserId _getUserByUserId,
            AddUser _addUser,
            AddUserWorkExperience _addUserWorkExperience,
            GetUserWorkExperience _getUserWorkExperience,
            UpdateUserWorkExperience _updateUserWorkExperience,
            DeleteUserWorkExperience _deleteUserWorkExperience,
            GetUserInterests _getUserInterests,
            AddUserInterest _addUserInterest,
            UpdateUserInterest _updateUserInterest,
            DeleteUserInterest _deleteUserInterest,
            GetUserSkills _getUserSkills,
            AddUserSkill _addUserSkill,
            UpdateUserSkill _updateUserSkill,
            DeleteUserSkill _deleteUserSkill,
            UpdateUserInfo _updateUserInfo
        )
        {
            getUsers = _getUsers;
            getUserAuthoredTrainings = _getUserAuthoredTrainings;
            getUserCompletedTrainings = _getUserCompletedTrainings;
            getUsersByTraining = _getUsersByTraining;
            getUserByUserNameAndPassword = _getUserByUserNameAndPassword;
            getUserByUserId = _getUserByUserId;
            addUser = _addUser;
            addUserWorkExperience = _addUserWorkExperience;
            getUserWorkExperience = _getUserWorkExperience;
            updateUserWorkExperience = _updateUserWorkExperience;
            deleteUserWorkExperience = _deleteUserWorkExperience;
            getUserInterests = _getUserInterests;
            addUserInterest = _addUserInterest;
            updateUserInterest = _updateUserInterest;
            deleteUserInterest = _deleteUserInterest;
            getUserSkills = _getUserSkills;
            addUserSkill = _addUserSkill;
            updateUserSkill = _updateUserSkill;
            deleteUserSkill = _deleteUserSkill;
            updateUserInfo = _updateUserInfo;
        }

        [HttpGet]
        public IActionResult Get()
        {
            List<User> users = getUsers.Execute();
            return Ok(users);
        }

        [HttpPost]
        public IActionResult Post(User user)
        {
            addUser.Execute(user);
            CreateUserDirectory(user.UserName);
            return Ok();
        }

        [HttpGet]
        [Route("UsersByTraining/{trainingId}")]
        public IActionResult UsersByTraining(int trainingId)
        {
            List<BasicUser> users = getUsersByTraining.Execute(trainingId);
            return Ok(users);
        }

        [HttpGet]
        [Route("{username}/{password}")]
        public IActionResult GetUserByUserNameAndPassword(string username, string password)
        {
            User user = getUserByUserNameAndPassword.Execute(username, password).FirstOrDefault();
            return Ok(user);
        }

        [HttpPut]
        [Route("{user}/{id}")]
        public IActionResult UpdateUserInfo(User user, int id)
        {
            updateUserInfo.Execute(user, id);
            return Ok();
        }

        [HttpGet]
        [Route("{userid}")]
        public IActionResult GetUserByUserId(int userid)
        {
            User user = getUserByUserId.Execute(userid).FirstOrDefault();
            return Ok(user);
        }

        [HttpGet]
        [Route("employment/{userId}")]
        public IActionResult GetUserEmploymentHistory(int userId)
        {
            return Ok(getUserWorkExperience.Execute(userId));
        }

        [HttpPost]
        [Route("employment")]
        public IActionResult AddToUserEmploymentHistory([FromBody]UserWorkExperience userWorkExperience)
        {
            addUserWorkExperience.Execute(userWorkExperience);
            return Ok();
        }

        //[HttpPut]
        //[Route("employment/{userId}")]
        //public IActionResult EditUserEmploymentHistory(int userId)
        //{
        //    return Ok(editUserWorkExperienceItem.Execute(userId));
        //}

        //[HttpDelete]
        //[Route("employment/{userId}/{experienceId}")]
        //public IActionResult DeleteFromUserEmploymentHistory(int userId, int experienceId)
        //{
        //    deleteFromUserWorkExperience.Execute(userId, experienceId);
        //    return Ok();
        //}

        [HttpGet]
        [Route("interests/{userId}")]
        public IActionResult GetUserInterests(int userId)
        {
            return Ok(getUserInterests.Execute(userId));
        }

        [HttpPost]
        [Route("interests")]
        public IActionResult AddUserInterest(UserInterest userInterest)
        {
            addUserInterest.Execute(userInterest);

            return Ok();
        }

        [HttpPut]
        [Route("interests/{Id}")]
        public IActionResult UpdateUserInterest(UserInterest userInterest, int Id)
        {
            updateUserInterest.Execute(userInterest, Id);

            return Ok();
        }

        [HttpDelete]
        [Route("interests/{Id}")]
        public IActionResult DeleteUserInterest(int Id)
        {
            deleteUserInterest.Execute(Id);

            return Ok();
        }

        [HttpGet]
        [Route("{id}/skills")]
        public IActionResult GetUserSkills(int id)
        {
            return Ok(getUserSkills.Execute(id));
        }

        [HttpPost]
        [Route("skills")]
        public IActionResult AddUserSkill(UserSkill userSkill)
        {
            addUserSkill.Execute(userSkill);

            return Ok();
        }

        [HttpPut]
        [Route("skills/{Id}")]
        public IActionResult UpdateUserSkill(UserSkill userSkill, int Id)
        {
            updateUserSkill.Execute(userSkill, Id);

            return Ok();
        }


        [HttpDelete]
        [Route("skills/{Id}")]
        public IActionResult DeleteUserSkill(int Id)
        {
            deleteUserSkill.Execute(Id);

            return Ok();
        }

        [HttpGet]
        [Route("{id}/experiences")]
        public IActionResult GetUserWorkExperiences(int id)
        {
            return Ok(getUserWorkExperience.Execute(id));
        }

        [HttpPost]
        [Route("experiences")]
        public IActionResult AddUserWorkExperience(UserWorkExperience userWorkExperience)
        {
            addUserWorkExperience.Execute(userWorkExperience);

            return Ok();
        }

        [HttpPut]
        [Route("experiences/{Id}")]
        public IActionResult UpdateUserWorkExperience(UserWorkExperience userWorkExperience, int Id)
        {
            updateUserWorkExperience.Execute(userWorkExperience, Id);

            return Ok();
        }


        [HttpDelete]
        [Route("experiences/{Id}")]
        public IActionResult DeleteUserWorkExperience(int Id)
        {
            deleteUserWorkExperience.Execute(Id);

            return Ok();
        }

        [HttpGet]
        [Route("{id}/AuthoredTrainings")] 
        public IActionResult GetUserAuthoredTrainings(int id)
        {
            return Ok(getUserAuthoredTrainings.Execute(id));
        }

        [HttpGet]
        [Route("{id}/CompletedTrainings")]
        public IActionResult GetUserCompletedTrainings(int id)
        {
            return Ok(getUserCompletedTrainings.Execute(id));
        }

        public void CreateUserDirectory(string  user)
        {
            string rootPath = $@"C:\Users\Seliger\source\repos\level-up\iye_backend\EmployeeAppBack";
            var path = Path.Combine(Path.Combine(rootPath, "UserFolders"), user);
            Directory.CreateDirectory(path);
        }

        [HttpPost]
        [Route("editphoto")]
        public  IActionResult SetProfilePhoto([FromBody]Paths paths)
        {
            var source = paths.theFile;

            var destination = paths.DestinationPath;

            if (source.FileAsBase64.Contains(","))
            {
                source.FileAsBase64 = source.FileAsBase64.Substring(source.FileAsBase64.IndexOf(",") + 1);
            }

            source.FileAsByteArray = Convert.FromBase64String(source.FileAsBase64);

            using (var fs = new FileStream(destination, FileMode.CreateNew))
            {
                fs.Write(source.FileAsByteArray, 0, source.FileAsByteArray.Length);
            }


            //var fileExists = Exists(source.FileName);
            //if (fileExists) Copy(source.FileName, destination);

            return Ok();
        }
    }
}