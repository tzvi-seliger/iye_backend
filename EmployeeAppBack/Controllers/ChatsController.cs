﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.Chats;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EmployeeAppBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class ChatsController : ControllerBase
    {
        public AddChat addChat;
        public DeleteChatItem deleteChatItem;
        public GetAllUserToUserChats getAllUserToUserChats;
        public UpdateChatItem updateChatItem;

        public ChatsController(
         AddChat _addChat,
         DeleteChatItem _deleteChatItem,
         GetAllUserToUserChats _getAllUserToUserChats,
         UpdateChatItem _updateChatItem
        )
        {
            addChat = _addChat;
            deleteChatItem = _deleteChatItem;
            getAllUserToUserChats = _getAllUserToUserChats;
            updateChatItem = _updateChatItem;
        }

        [HttpGet]
        [Route("{senderid}/{recipientid}")]
        public IActionResult Get(int senderid, int recipientid)
        {
            List<Chat> chats = getAllUserToUserChats.Execute(senderid, recipientid);
            return Ok(chats);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Chat chat)
        {
            addChat.Execute(chat);
            return Ok();
        }

        [HttpPut]
        [Route("{text}/{id}")]
        public IActionResult Update(string text, int id)
        {
            updateChatItem.Execute(text, id);
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            deleteChatItem.Execute(id);
            return Ok();
        }

    }
}