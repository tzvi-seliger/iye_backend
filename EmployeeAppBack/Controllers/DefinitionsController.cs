﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.Definitions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeAppBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class DefinitionsController : ControllerBase
    {
        public AddTermWithDefinition addTermWithDefinition;
        public GetTermDefinition getTermDefinition;
        public GetTermDefinitions getTermDefinitions;
        public UpdateTermDefinition updateTermDefinition;

        public DefinitionsController(
            AddTermWithDefinition _addTermWithDefinition,
            GetTermDefinition _getTermDefinition,
            GetTermDefinitions _getTermDefinitions,
            UpdateTermDefinition _updateTermDefinition
        )
        {
            addTermWithDefinition = _addTermWithDefinition;
            getTermDefinition = _getTermDefinition;
            getTermDefinitions = _getTermDefinitions;
            updateTermDefinition = _updateTermDefinition;
        }

        [HttpPost]
        public IActionResult AddDefinition([FromBody] TermDefinition termDefinition)
        {
            addTermWithDefinition.Execute(termDefinition);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetTermDefinitions()
        {
            var termDefinitions = getTermDefinitions.Execute();
            return Ok(termDefinitions);
        }

        [HttpGet]
        [Route("TermDefinition/{term}")]
        public IActionResult GetTermDefinition(string term)
        {
            var termDefinition = getTermDefinition.Execute(term);
            return Ok(termDefinition);
        }

        [HttpPut]
        public IActionResult UpdateTermDefinition([FromBody] TermDefinition termDefinition)
        {
            updateTermDefinition.Execute(termDefinition);
            return Ok();
        }
    }
}