﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace EmployeeAppBack.Controllers
{
    [Route("api/articles")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class ArticlesController : ControllerBase
    {
        //private string AccountName { get; set; } = "Amazon";

        public AddArticle addArticle;
        public EditArticle editArticle;
        public GetArticleById getArticleById;
        public GetArticleTitles getArticleTitles;
        public GetArticleTitlesByKeyword getArticleTitlesByKeyword;
        public DeleteArticle deleteArticle;
        public GetArticleAuthors getArticleAuthors;
        public GetArticleLevels getArticleLevels;
        public GetArticleRatings getArticleRatings;
        public GetArticleSubjects getArticleSubjects;
        public GetArticleTitlesFiltered getArticleTitlesFiltered;
        public GetArticlesByAuthorId getArticlesByAuthorId;

        public ArticlesController(
         AddArticle _addArticle,
         EditArticle _editArticle,
         GetArticleById _getArticleById,
         GetArticleTitles _getArticleTitles,
         GetArticleTitlesByKeyword _getArticleTitlesByKeyword,
         DeleteArticle _deleteArticle,
         GetArticleAuthors _getArticleAuthors,
         GetArticleLevels _getArticleLevels,
         GetArticleRatings _getArticleRatings,
         GetArticleSubjects _getArticleSubjects,
         GetArticleTitlesFiltered _getArticleTitlesFiltered,
         GetArticlesByAuthorId _getArticlesByAuthorId
        )
        {
            addArticle = _addArticle;
            editArticle = _editArticle;
            getArticleById = _getArticleById;
            getArticleTitles = _getArticleTitles;
            getArticleTitlesByKeyword = _getArticleTitlesByKeyword;
            deleteArticle = _deleteArticle;
            getArticleAuthors = _getArticleAuthors;
            getArticleLevels = _getArticleLevels;
            getArticleRatings = _getArticleRatings;
            getArticleSubjects = _getArticleSubjects;
            getArticleTitlesFiltered = _getArticleTitlesFiltered;
            getArticlesByAuthorId = _getArticlesByAuthorId;
        }

        [HttpGet]
        public IActionResult Get()
        {
            List<ArticleBase> articleHeaders = getArticleTitles.Execute();
            return Ok(articleHeaders);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Article article)
        {
            addArticle.Execute(article);
            return Ok();
        }

        [HttpPost]
        [Route("update-article")]
        public IActionResult Update([FromBody] Article article)
        {
            editArticle.Execute(article);
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(string id)
        {
            deleteArticle.Execute(id);
            return Ok();
        }

        [HttpGet]
        [Route("authors")]
        public IActionResult GetAuthors()
        {
            List<string> articleAuthors = getArticleAuthors.Execute();
            return Ok(articleAuthors);
        }

        [HttpGet]
        [Route("subjects")]
        public IActionResult GetSubjects()
        {
            List<string> articleSubjects = getArticleSubjects.Execute();
            return Ok(articleSubjects);
        }

        [HttpGet]
        [Route("ratings")]
        public IActionResult GetRatings()
        {
            List<int> articleRatings = getArticleRatings.Execute();
            return Ok(articleRatings);
        }

        [HttpGet]
        [Route("Levels")]
        public IActionResult GetLevels()
        {
            List<string> articleLevels = getArticleLevels.Execute();
            return Ok(articleLevels);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetArticle(int id)
        {
            ArticleBase article = getArticleById.Execute(id);
            return Ok(article);
        }

        [HttpGet]
        [Route("GetArticlesByAuthorId/{id}")]
        public IActionResult GetArticlesByAuthorId(int id)
        {
            List<FullArticle> article = getArticlesByAuthorId.Execute(id);
            return Ok(article);
        }

        [HttpGet]
        [Route("ArticleByKeyword/{key}")]
        public IActionResult GetArticleByKeyword(string key)
        {
            List<ArticleBase> articleHeaders = getArticleTitlesByKeyword.Execute(key);
            return Ok(articleHeaders);
        }

        [HttpPost]
        [Route("FilteredArticles")]
        public IActionResult GetFilteredArticles([FromBody] ArticleFilters articleFilters)
        {
            List<ArticleBase> filteredArticles = getArticleTitlesFiltered.Execute(articleFilters);
            return Ok(filteredArticles);
        }
    }
}