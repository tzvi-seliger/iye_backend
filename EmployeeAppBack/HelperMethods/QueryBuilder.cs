﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.HelperMethods
{
    public class QueryBuilder<T> where T : new()
    {
        public static string Select(List<string> itemsToRetreive, string propName, string setValue, string propDataType, string tableName)
        {
            string queryItems;

            if (itemsToRetreive.Count.Equals(0)) queryItems = " * ";
            else
            {
                queryItems = string.Join(",", itemsToRetreive);
            }
            string valueToSet = propDataType.Equals("int") ? setValue : $"'{setValue}'";

            return $"SELECT {queryItems} FROM {tableName} WHERE {propName} = {valueToSet}";
        }

        public static string Insert(List<string> propsToInsert, List<string> valuesToSetProps, string propName, string setValue, string propDataType, string tableName)
        {
            string getFields = $"SELECT NAME FROM sys.columns WHERE object_id = OBJECT_ID('{tableName}')";
            string queryItems = string.Join(",", propsToInsert);

            return $"Insert into {tableName} ({queryItems}) values ({valuesToSetProps})";
        }

        public static string Delete(string Id)
        {
            return $"Delete from Articles WHERE Id = {Id}";
        }

        public static string Update()
        {
            return string.Empty;
            //return $"UPDATE Articles SET Title='{article.Title}', Description='{article.Description}', Content='{article.Content}', Author='{article.Author}',  Level='{article.Level}',  Subject='{article.Subject}',  Rating={article.Rating} WHERE Id = {article.Id}";
        }

        public static T ExecuteGeneric(string query, string connectionString)
        {
            T obj = new T();
            var props = typeof(T).GetProperties();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(query, conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.Name.Equals("Int32"))
                        {
                            prop.SetValue(obj, Convert.ToInt32(reader[prop.Name.ToString()]));
                        }
                        if (prop.PropertyType.Name.Equals("String"))
                        {
                            prop.SetValue(obj, Convert.ToString(reader[prop.Name.ToString()]));
                        }
                    }
                }
            }
            return obj;
        }
        public static List<T> ExecuteGenericList(string query, string connectionString)
        {
            List<T> obj = new List<T>();
            var props = typeof(T).GetProperties();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(query, conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    T item = new T();

                    foreach (var prop in props)
                    {
                        if (prop.PropertyType.Name.Equals("Int32"))
                        {
                            prop.SetValue(item, Convert.ToInt32(reader[prop.Name.ToString()]));
                        }

                        if (prop.PropertyType.Name.Equals("String"))
                        {
                            prop.SetValue(item, Convert.ToString(reader[prop.Name.ToString()]));
                        }
                    }
                    obj.Add(item);
                }
            }
            return obj;
        }
    }
}
