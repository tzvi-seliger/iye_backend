﻿using System.IO;

namespace EmployeeAppBack.Infrastructure
{
    public static class FileSystem
    {
        public static bool DoesFileExist(string filePath)
        {
            if (File.Exists(filePath))
            {
                return true;
            }
            return false;
        }
    }
}