﻿# IYE Documentation

## Technology Choices

### Hosting & Registrar

### Animation & Design
#### Frameworks & Libraries
#### Tools

### Backend Code Framework/Language
#### Continuous Integration/Continuous Development - Devops

https://www.altexsoft.com/blog/api-testing/

types of tests

functional tests
performance testing
security testing

#### Testing API Components
use xunit to start with

#### Testing API endpoints
postman js testing library
https://learning.postman.com/docs/writing-scripts/test-scripts/
powershell?

### Frontend Framework
### Containerization/Microservices Architecture
### Security Frameworks
### Documentation
### Database (Platform)
What issues are we anticipating

Which Databases to choose
Do we use a framework or code directly?
currently using Angular 
what are the pros and cons and are there significantly better options
is it good for large projects?
https://www.freecodecamp.org/news/do-we-still-need-javascript-frameworks-42576735949b/


How to split a monolith into microservices
best way to stream video in a web application
which host to use or to self host to start
which registrar to use
linode.com
use containerization
which version of linux to use?


which backend language
text and video chat integrations

good libraries for animations and layout for angular
https://dev.to/frostqui/my-favorite-angular-libraries-to-use-in-any-project-47ai
https://angular.io/guide/animations
## Business plan
### Profitability
### Growing Customer Base
### Long Term Growth Goals
### Financing

## General Concept

## Database design
what entites will be needed 
what is the idea

Anyone can become a user
a user can select trainings they are interested in, engae in trainings create trainings fork trainings
add skills and interests. job experience work portfolio items
 A business can have an account.
 They can own their own trainings
 allow users to become part of their account as employees
 assgn trainings to their employees
 utilize other companies trainings
 share their own trainings with other companies
trainings will be created by users
a training can utilize any section or a whole training within their own training
links to youtube, other services or links online
employees can voew their team structure add positions sift thru their network for good candidates
sift thru the whole user base for good candidates

## Endpoints

## Frontend pages

1. where you can see a snapshot of the employees profile
experience
skills
interests
portfolio
Trainings completed

2. Trainings Page
Search entire trainings 
advanced filters
Create new Training Form

3. Individual Trainings page

4. Play around with company hierarchy 
create profile for position and requirements 
get suggestions from users and your own employees

5. Individual Training section page - text file, video etc.

6. Company account profile page

7. Homepage - Signup/Login Form

## Security




