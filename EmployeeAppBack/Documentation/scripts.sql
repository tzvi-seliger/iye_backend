﻿/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ID]
      ,[UserId]
      ,[Title]
      ,[Detail]
      ,[SpanStart]
      ,[SpanEnd]
  FROM [Employee_Management].[dbo].[PortfolioItems]

  -- get full portfolio for user
  -- add portfolio item for user
  -- update portfolio item for user
  -- delete portfolio item for user

  select * from PortfolioItems a full outer join AlbumItems b on a.Id = b.PortfolioId where a.UserId = 1

  select * from PortfolioItems where UserId = 1

  select * from AlbumItems WHERE PortfolioId = 1

  update PortfolioItems 
  SET UserId = 1,
  Title = 'Employee Management Website', 
  Detail = 'Give your people opportunity to grow exponentially',
  SpanStart = '2022-05-24 02:32:55',
  SpanEnd ='2022-12-24 02:32:55'
  WHERE Id = 1 AND UserId = 1
  
  update PortfolioItems 
  SET UserId = 1,
  Title = 'Employee Management Website', 
  Detail = 'Give your people opportunity to grow exponentially',
  SpanStart = '2022-05-24 02:32:55',
  SpanEnd ='2022-12-24 02:32:55'
  WHERE Id = 1 AND UserId = 1

  INSERT INTO PortfolioItems
  (UserId, Title, Detail, SpanStart, SpanEnd) VALUES
  (1, 'Employee Management Website', 'Give your people opportunity to grow exponentially', '2022-05-24 02:32:55', '2022-12-24 02:32:55');

  insert into AlbumItems(PortfolioId, PhotoPath) values (1, 'http://photo.path')