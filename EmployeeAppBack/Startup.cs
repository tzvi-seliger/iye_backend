﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries;
using EmployeeAppBack.Queries.Chats;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace EmployeeAppBack

//TODO implement and test sessions and login
//TODO implement hashing and salting passwords
//TODO research security measures

//TODO hierarchy within a company
//TODO hierarchyin trainings, which trainings are part of other trainings
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Add CORS policy
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //string developconnstring = @"Data Source=localhost;Initial Catalog=Employee_Management;Integrated Security=True";
            //string connstring = @"Server = tcp:emp-management-server.database.windows.net,1433; Initial Catalog=emp; Persist Security Info = False; User ID =tzviadmin;Password=gdisgreatandiamtherebygreat1!; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
            string developconnstring = @"Data Source = db982997332.hosting-data.io,1433; Initial Catalog=db982997332; Persist Security Info = False; User ID =dbo982997332;Password=whatthe7; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;";
            //MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False; Connection Timeout = 30;
            //Provider = sqloledb; Data Source = db982997332.hosting - data.io,1433; Initial Catalog = db982997332; User Id = ; Password = *****;
            services.AddTransient(j => new Queries.GetTrainings(developconnstring));
            services.AddTransient<Query>(j => new Query(developconnstring));
            services.AddTransient<QueryReturn>(j => new QueryReturn(developconnstring));
            services.AddTransient(j => new Queries.TrainingsByAccount(developconnstring));
            services.AddTransient(j => new Queries.Users.GetEmployeeTrainingsByUserId(developconnstring));
            services.AddTransient(j => new Queries.Users.Trainings.GetUserAuthoredTrainings(developconnstring));
            services.AddTransient(j => new Queries.Users.Trainings.GetUserCompletedTrainings(developconnstring));
            services.AddTransient(j => new Queries.Users.Trainings.AddUserTraining(developconnstring));
            services.AddTransient(j => new Queries.Users.Trainings.GetEmployeeTrainingsById(developconnstring));
            services.AddTransient(j => new Queries.Users.GetUsersByTraining(developconnstring));
            services.AddTransient(j => new Queries.Users.UpdateUserInfo(developconnstring));
            services.AddTransient(j => new Queries.Users.GetUsers(developconnstring));
            services.AddTransient(j => new Queries.Users.GetUserByUserNameAndPassword(developconnstring));
            services.AddTransient(j => new Queries.Users.GetUserByUserId(developconnstring));
            services.AddTransient(j => new Queries.Users.AddUser(developconnstring));
            services.AddTransient(j => new Queries.Users.WorkExperience.AddUserWorkExperience(developconnstring));
            services.AddTransient(j => new Queries.Users.WorkExperience.GetUserWorkExperience(developconnstring));
            services.AddTransient(j => new Queries.Users.WorkExperience.UpdateUserWorkExperience(developconnstring));
            services.AddTransient(j => new Queries.Users.WorkExperience.DeleteUserWorkExperience(developconnstring));
            services.AddTransient(j => new Queries.Users.Interests.GetUserInterests(developconnstring));
            services.AddTransient(j => new Queries.Users.Interests.AddUserInterest(developconnstring));
            services.AddTransient(j => new Queries.Users.Interests.UpdateUserInterest(developconnstring));
            services.AddTransient(j => new Queries.Users.Interests.DeleteUserInterest(developconnstring));
            services.AddTransient(j => new Queries.Users.Skills.UpdateUserSkill(developconnstring));
            services.AddTransient(j => new Queries.Users.Skills.AddUserSkill(developconnstring));
            services.AddTransient(j => new Queries.Users.Skills.GetUserSkills(developconnstring));
            services.AddTransient(j => new Queries.Users.Skills.DeleteUserSkill(developconnstring));
            services.AddTransient(j => new Queries.Users.Hierarchy.GetRootManagerIdByAccountId(developconnstring));
            services.AddTransient(j => new Queries.Users.Hierarchy.GetEmployeesByManagerId(developconnstring));
            services.AddTransient(j => new Queries.Users.Hierarchy.GetAllUsersInAccount(developconnstring));
            services.AddTransient(j => new Queries.Users.Hierarchy.GetAllManagerProfiles(developconnstring));
            services.AddTransient(j => new Queries.AddArticle(developconnstring));
            services.AddTransient(j => new Queries.GetArticleById(developconnstring));
            services.AddTransient(j => new Queries.GetArticleTitles(developconnstring));
            services.AddTransient(j => new Queries.GetArticleTitlesByKeyword(developconnstring));
            services.AddTransient(j => new Queries.GetArticleAuthors(developconnstring));
            services.AddTransient(j => new Queries.GetArticleLevels(developconnstring));
            services.AddTransient(j => new Queries.GetArticleRatings(developconnstring));
            services.AddTransient(j => new Queries.GetArticleSubjects(developconnstring));
            services.AddTransient(j => new Queries.GetArticlesByAuthorId(developconnstring));
            services.AddTransient(j => new Queries.GetArticleTitlesFiltered(developconnstring));
            services.AddTransient(j => new Queries.EditArticle(developconnstring));
            services.AddTransient(j => new Queries.DeleteArticle(developconnstring));
            services.AddTransient(j => new Queries.Definitions.AddTermWithDefinition(developconnstring));
            services.AddTransient(j => new Queries.Definitions.GetTermDefinition(developconnstring));
            services.AddTransient(j => new Queries.Definitions.GetTermDefinitions(developconnstring));
            services.AddTransient(j => new Queries.Definitions.UpdateTermDefinition(developconnstring));
            services.AddTransient(j => new Queries.Positions.AddPosition(developconnstring));
            services.AddTransient(j => new Queries.Positions.DeletePosition(developconnstring));
            services.AddTransient(j => new Queries.Positions.GetPositions(developconnstring));
            services.AddTransient(j => new Queries.Positions.EditPosition(developconnstring));
            services.AddTransient(j => new Queries.Trainings.GetLastTrainingNumber(developconnstring));
            services.AddTransient(j => new Queries.Trainings.GetTrainingById(developconnstring));
            services.AddTransient(j => new Queries.Accounts.GetAccountById(developconnstring));
            services.AddTransient(j => new Queries.Accounts.GetAccounts(developconnstring));
            services.AddTransient(j => new Queries.Accounts.AddAccount(developconnstring));
            services.AddTransient(j => new Queries.Accounts.DeleteAccountById(developconnstring));
            services.AddTransient(j => new Queries.Accounts.UpdateAccountById(developconnstring));
            services.AddTransient(j => new Queries.Chats.AddChat(developconnstring));
            services.AddTransient(j => new Queries.Chats.DeleteChatItem(developconnstring));
            services.AddTransient(j => new Queries.Chats.GetAllUserToUserChats(developconnstring));
            services.AddTransient(j => new Queries.Chats.UpdateChatItem(developconnstring));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            //app.UseCorsMiddleware();

            app.UseCors("CorsPolicy");
        }
    }
}