﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class TermDefinition
    {
        public string Term { get; set; }
        public string Definition { get; set; }
    }
}
