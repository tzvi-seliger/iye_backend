﻿namespace EmployeeAppBack.Models
{
    public class PostUserTraining
    {
        public int UserId { get; set; }
        public int TrainingId { get; set; }
        public string TrainingStatus { get; set; }
    }
}