﻿namespace EmployeeAppBack.Models
{
    public class EmployeeTraining
    {
        public int UserId { get; set; }
        public int TrainingId { get; set; }
        public string TrainingName { get; set; }
        public string TrainingStatus { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
    }
}