﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class ArticleFilters
    {
        public List<string> Ratings { get; set; } = new List<string>();
        public List<string> Subjects { get; set; } = new List<string>();
        public List<string> Levels { get; set; } = new List<string>();
        public List<string> Authors { get; set; } = new List<string>();
    }
}
