﻿using System;

namespace EmployeeAppBack.Models
{
    public class UserWorkExperience
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public string WorkTitle { get; set; }
        public string WorkDescription { get; set; }
        public DateTime WorkStartDate { get; set; }
        public DateTime WorkEndDate { get; set; }
    }
}