﻿using Newtonsoft.Json;

namespace EmployeeAppBack.Models
{
    public class UserHierarchy
    {
        public int UserId { get; set; }

        public int? ManagerId { get; set; } = null;

        public int AccountId { get; set; }

        public string UserType { get; set; }

        public string UserName { get; set; }

        public string PasswordString { get; set; }

        public string Salt { get; set; }

        public string UserEmailAddress { get; set; }

        public string UserFirstName { get; set; }

        public string UserLastName { get; set; }

        public string UserPhoneNumber { get; set; }

        public string ProfilePhotoFilepath { get; set; }
    }
}