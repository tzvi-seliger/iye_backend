﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class Position
    {
        public int PositionId { get; set; } = 0;
        public int UserId { get; set; } = 0;
        public int AccountID { get; set; }
        public int SalaryMin { get; set; }
        public int SalaryMax { get; set; }
        public string Location { get; set; }
        public Boolean IsRemote { get; set; }
        public int ExperienceRequired { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }



    }
}
