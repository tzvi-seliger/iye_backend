﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class GetUserInterest
    {
        //public int AccountId { get; set; }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Interest { get; set; }
        public string Description { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
    }
}
