﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class Subject
    {
        /// <summary>
        /// Id of the subject
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the name of the subject
        /// </summary>
        public string SubjectName { get; set; }
    }
}
