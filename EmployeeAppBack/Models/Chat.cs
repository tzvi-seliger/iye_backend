﻿namespace EmployeeAppBack.Models
{
    public class Chat
    {
        public int? Id { get; set; } = 0;
        public string Text { get; set; }
        public int SenderUserId { get; set; }
        public int RecipientUserId { get; set; }

    }
}
