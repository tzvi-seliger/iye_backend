﻿namespace EmployeeAppBack.Models
{
    public class Article : ArticleBase
    {
        public string Content { get; set; }
        public int Rating { get; set; }
        public string Level { get; set; }
        public string Subject { get; set; }
        public string Author { get; set; }

        
    }
}