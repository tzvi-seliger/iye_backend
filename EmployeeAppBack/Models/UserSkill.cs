﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class UserSkill
    {
        //public int AccountId { get; set; }
        public int? Id { get; set; } = default(int?);
        public int UserId { get; set; }
        public string Skill { get; set; }
        public string Description { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
    }
}
