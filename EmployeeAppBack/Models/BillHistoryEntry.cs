﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Models
{
    public class BillHistoryEntry
    {
        public int UserId { get; set; }
        public int CallingPartyId { get; set; }
        public DateTime CallTimeStamp { get; set; }
        public int RateAtTimeOfCall { get; set; }
    }
}
