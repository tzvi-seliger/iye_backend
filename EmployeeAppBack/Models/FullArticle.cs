﻿using EmployeeAppBack.Services;
using System.Reflection.Emit;

namespace EmployeeAppBack.Models
{
    public class FullArticle 
    {
        public int Id { get; set; } = 0;
        public string Title   { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public int Rating { get; set; }
        public string Author { get; set; } = string.Empty;
        public string Level { get; set; } = string.Empty;
        public string Subject { get; set; } = string.Empty;
        public int AuthorId { get; set; } = 0;
        public int UserID { get; set; } = 0;
        public int AccountID { get; set; } = 0;
        public string UserType { get; set; } = string.Empty;
        public string UserName { get; set; } = string.Empty;
        public string UserEmailAddress { get; set; } = string.Empty;
        public string UserFirstName { get; set; } = string.Empty;
        public string UserLastName { get; set; } = string.Empty;
        public string UserPhoneNumber { get; set; } = string.Empty;
        public int ManagerId { get; set; } = 0;
        public string ProfilePhotoFilepath { get; set; } = string.Empty;
    }
}