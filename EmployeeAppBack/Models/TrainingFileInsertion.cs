﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace EmployeeAppBack.Models
{
    public class TrainingFileInsertion
    {
        [Required]
        public int TrainingId { get; set; }

        public int? OrderNo { get; set; } = null;

        [Required]
        public string Description { get; set; }

        public string FilePath { get; set; }

        [Required]
        public bool IsComposite { get; set; }

        public int TrainingIdReference { get; set; }
        public int OrderNoReference { get; set; }
        public IFormFile File { get; set; }
    }
}