﻿namespace EmployeeAppBack.Models
{
    public class BasicUser
    {
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
    }
}