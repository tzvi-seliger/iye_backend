﻿namespace EmployeeAppBack.Models
{
    public class ArticleBase
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}