﻿namespace EmployeeAppBack.Models
{
    public class TrainingFile
    {
        public int TrainingId { get; set; }
        public string TrainingName { get; set; }
        public string AccountName { get; set; }
        public int OrderNo { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public bool IsComposite { get; set; }
        public int TrainingIdReference { get; set; }
        public int OrderNoReference { get; set; }
    }
}