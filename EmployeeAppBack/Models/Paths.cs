﻿namespace EmployeeAppBack.Models
{
    public class Paths
    {
        public FileToUpload theFile { get; set; }
        public string DestinationPath { get; set; }
    }
}
