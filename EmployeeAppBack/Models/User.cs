﻿using Newtonsoft.Json;

namespace EmployeeAppBack.Models
{
    public class User
    {
        public int UserId { get; set; }

        [JsonProperty("ManagerId")]
        public int ManagerUserId { get; set; }

        public int AccountId { get; set; }

        public string UserType { get; set; }

        public string UserName { get; set; }

        public string PasswordString { get; set; }

        public string Salt { get; set; }

        public string UserEmailAddress { get; set; }

        public string UserFirstName { get; set; }

        public string UserLastName { get; set; }

        public string UserPhoneNumber { get; set; }

        public string ProfilePhotoFilepath { get; set; }
    }
}