﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Queries.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Services
{
    public class Accounts
    {
        GetAccounts _getAccounts;
        GetAccountById _getAccountById;
        AddAccount _addCount;
        DeleteAccountById _deleteAccountById;
        UpdateAccountById _updateAccountById;



        public Accounts(
            GetAccounts getAccounts,
            GetAccountById getAccountById,
            AddAccount addCount,
            DeleteAccountById deleteAccountById,
            UpdateAccountById updateAccountById
        )
        {
            _getAccountById = getAccountById;
            _getAccounts = getAccounts;
            _addCount = addCount;
            _deleteAccountById = deleteAccountById;
            _updateAccountById = updateAccountById;
        }

        public List<Account> GetAccounts()
        {
            List<Account> accounts = _getAccounts.Execute();
            return accounts;
        }

        public Account GetAccountById(int accountId)
        {
            Account account = _getAccountById.Execute(accountId);
            return account;
        }

        public void AddAccount(Account account)
        {
            _addCount.Execute(account);
        }

        public void DeleteAccountById(int accountId)
        {
            _deleteAccountById.Execute(accountId);
        }

        public void UpdateAccountById(Account account)
        {
            _updateAccountById.Execute(account);
        }

        //Employees - should this be in its own controller or in the accounts controller
        //Retention Statistics
        //Positions available within company - should this be in its own controller or in the Positions controller
        //matching candidates for positions - should this be in its own controller or in the Positions controller



    }
}
