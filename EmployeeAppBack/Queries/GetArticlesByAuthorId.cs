﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetArticlesByAuthorId
    {
        private readonly string connectionString;

        public GetArticlesByAuthorId(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<FullArticle> Execute(int Id)
        {
            List<FullArticle> articles = new List<FullArticle>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    articles.Add(new FullArticle
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        Title = Convert.ToString(reader["Title"]),
                        Description = Convert.ToString(reader["Description"]),
                        Content = Convert.ToString(reader["Content"]),
                        Rating  = Convert.ToInt32(reader["Rating"]),
                        Author  = Convert.ToString(reader["Author"]),
                        Level = Convert.ToString(reader["Level"]),
                        Subject = Convert.ToString(reader["Subject"]),
                        AuthorId  = Convert.ToInt32(reader["AuthorId"]),
                        UserID = Convert.ToInt32(reader["UserID"]),
                        AccountID = Convert.ToInt32(reader["AccountID"]),
                        UserType  = Convert.ToString(reader["UserType"]),
                        UserName  = Convert.ToString(reader["UserName"]),
                        UserEmailAddress = Convert.ToString(reader["UserEmailAddress"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                        UserPhoneNumber = Convert.ToString(reader["UserPhoneNumber"]),
                        ManagerId = Convert.ToInt32(reader["ManagerId"]),
                        ProfilePhotoFilepath  = Convert.ToString(reader["ProfilePhotoFilepath"]),
                    });
                }
            }
            return articles;
        }

        public string BuildQuery(int Id)
        {
            return $" select " +
                $"Id," +
                $"Title," +
                $"Description," +
                $"Content," +
                $"Rating, " +
                $"Author," +
                $"Level," +
                $"Subject, " +
                $"AuthorId," +
                $"UserId," +
                $"AccountId," +
                $"UserType, " +
                $"UserName, " +
                $"UserEmailAddress," +
                $"UserFirstName," +
                $"UserLastName," +
                $"UserPhoneNumber," +
                $"ManagerId," +
                $"ProfilePhotoFilePath" +
                $" from articles a join users b on a.authorId = b.UserID where b.userId = {Id}";
        }
    }
}