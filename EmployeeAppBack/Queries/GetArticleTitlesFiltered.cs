﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries
{
    public class GetArticleTitlesFiltered
    {
        private readonly string connectionString;

        public GetArticleTitlesFiltered(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<ArticleBase> Execute(ArticleFilters articleFilters)
        {
            List<ArticleBase> articles = new List<ArticleBase>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(articleFilters), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    articles.Add(new ArticleBase
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        Title = Convert.ToString(reader["Title"]),
                        Description = Convert.ToString(reader["Description"])
                    });
                }
            }
            return articles;
        }

        
        public string BuildQuery(ArticleFilters articleFilters)
        {
            string ratingsString = string.Empty;
            string levelsString = string.Empty;
            string subjectsString = string.Empty;
            string authorsString = string.Empty;

            if (!articleFilters.Ratings.Equals(null))
            {
                foreach (var rating in articleFilters.Ratings.Select((value, i) => new { i, value }))
                {
                    var value = rating.value;
                    var index = rating.i;

                    if (index.Equals(articleFilters.Ratings.Count - 1))
                    {
                        ratingsString += $"'{value}'";
                    }
                    else
                    {
                        ratingsString += $"'{value}',";
                    }
                }
            }

            if (!articleFilters.Levels.Equals(null))
            {
                foreach (var level in articleFilters.Levels.Select((value, i) => new { i, value }))
                {
                    var value = level.value;
                    var index = level.i;

                    if (index.Equals(articleFilters.Levels.Count - 1))
                    {
                        levelsString += $"' {value} '";
                    }
                    else
                    {
                        levelsString += $"'{value}',";
                    }
                }
            }

            if (!articleFilters.Subjects.Equals(null))
            {

                foreach (var subject in articleFilters.Subjects.Select((value, i) => new { i, value }))
                {
                    var value = subject.value;
                    var index = subject.i;

                    if (index.Equals(articleFilters.Subjects.Count - 1))
                    {
                        subjectsString += $"'{value}'";
                    }
                    else
                    {
                        subjectsString += $"'{value}',";
                    }
                }
            }

            if (!articleFilters.Authors.Equals(null))
            {

                foreach (var author in articleFilters.Authors.Select((value, i) => new { i, value }))
                {
                    var value = author.value;
                    var index = author.i;

                    if (index.Equals(articleFilters.Authors.Count - 1))
                    {
                        authorsString += $"'{value}'";
                    }
                    else
                    {
                        authorsString += $"'{value}',";
                    }
                }
            }

            var sql =
                $" SELECT" +
                $" Title, Description, Id" +
                $" FROM [dbo].[Articles]";

            if (articleFilters.Authors.Count > 0) {
                sql += $" WHERE Author IN ({authorsString})";
            }

            if (articleFilters.Ratings.Count > 0)
            {
                sql += $" AND Rating IN ({ratingsString})";
            }

            if (articleFilters.Levels.Count > 0)
            {
                sql += $" AND Level IN ({levelsString})";
            }

            if (articleFilters.Subjects.Count > 0)
            {
                sql += $" AND Subject IN ({subjectsString})";
            }

            return sql;
        }
    }
}