﻿using EmployeeAppBack.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;

namespace EmployeeAppBack.Queries.Users.Hierarchy
{
    public class GetAllManagerProfiles
    {
        private readonly string connectionString;

        public GetAllManagerProfiles(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<UserHierarchy> Execute(int accountId)
        {
            List<UserHierarchy> users = new List<UserHierarchy>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(accountId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new UserHierarchy()
                    {
                        UserEmailAddress = Convert.ToString(reader["UserEmailAddress"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        Salt = Convert.ToString(reader["Salt"]),
                        PasswordString = Convert.ToString(reader["PasswordString"]),
                        AccountId = Convert.ToInt32(reader["AccountId"]),
                        ManagerId = Convert.ToInt32(reader["ManagerUserId"]),
                        ProfilePhotoFilepath = Convert.ToString(reader["ProfilePhotoFilepath"]),
                        UserId = Convert.ToInt32(reader["UserId"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                        UserName = Convert.ToString(reader["UserName"]),
                        UserPhoneNumber = Convert.ToString(reader["UserPhoneNumber"]),
                        UserType = Convert.ToString(reader["UserType"])

                    });
                }
            }
            return users;
        }

        public string BuildQuery(int accountId)
        {
            return $"SELECT * FROM Users where AccountID = {accountId} and UserId IN (SELECT ManagerId From Users WHERE AccountId={accountId})";
        }
    }
}
