﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class AddUser
    {
        private readonly string connectionString;

        public AddUser(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(User user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(user), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(User user)
        {
            return $"insert into Users(AccountId, UserType, UserName, PasswordString, Salt, UserEmailAddress, UserFirstName, UserLastName, UserPhoneNumber) values({user.AccountId}, '{user.UserType}', '{user.UserName}', '{user.PasswordString}', '{user.Salt}', '{user.UserEmailAddress}', '{user.UserFirstName}', '{user.UserLastName}', '{user.UserPhoneNumber}')";
        }
    }
}