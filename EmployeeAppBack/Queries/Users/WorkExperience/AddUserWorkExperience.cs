﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.WorkExperience
{
    public class AddUserWorkExperience
    {
        private readonly string connectionString;

        public AddUserWorkExperience(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(UserWorkExperience userWorkExperience)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userWorkExperience), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(UserWorkExperience userWorkExperience)
        {
            return $"USE [db982997332]; " +
            $"INSERT INTO [dbo].[UserWorkExperiences]" +
            $"([AccountID]" +
            $",[UserId]" +
            $",[WorkTitle]" +
            $",[WorkDescription]" +
            $",[WorkStartDate]" +
            $" ,[WorkEndDate])" +
            $"VALUES" +
            $"( '{userWorkExperience.AccountId}'," +
            $" '{userWorkExperience.UserId}'," +
            $" '{userWorkExperience.WorkTitle}'," +
            $" '{userWorkExperience.WorkDescription}'," +
            $" '{userWorkExperience.WorkStartDate}'," +
            $" '{userWorkExperience.WorkEndDate}'); ";
        }
    }
}