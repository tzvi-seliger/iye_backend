﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.WorkExperience
{
    public class GetUserWorkExperience
    {
        private readonly string connectionString;

        public GetUserWorkExperience(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<UserWorkExperience> Execute(int userId)
        {
            List<UserWorkExperience> userWorkExperiences = new List<UserWorkExperience>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    userWorkExperiences.Add(new UserWorkExperience
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        AccountId = Convert.ToInt32(reader["AccountId"]),
                        UserId = Convert.ToInt32(reader["UserId"]),
                        WorkDescription = Convert.ToString(reader["WorkDescription"]),
                        WorkStartDate = Convert.ToDateTime(reader["WorkStartDate"]),
                        WorkEndDate = Convert.ToDateTime(reader["WorkEndDate"]),
                        WorkTitle = Convert.ToString(reader["WorkTitle"]),
                    });
                }
            }
            return userWorkExperiences;
        }

        public string BuildQuery(int userId)
        {
            return $@"SELECT
       [Id] 
      ,[AccountID]
      ,[UserId]
      ,[WorkTitle]
      ,[WorkDescription]
      ,[WorkStartDate]
      ,[WorkEndDate]
  FROM [db982997332].[dbo].[UserWorkExperiences]

  where UserId = {userId};";
        }
    }
}