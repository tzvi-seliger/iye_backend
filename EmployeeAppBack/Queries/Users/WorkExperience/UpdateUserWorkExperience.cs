﻿using EmployeeAppBack.Models;
using System;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.WorkExperience
{
    public class UpdateUserWorkExperience
    {
        private readonly string connectionString;

        public UpdateUserWorkExperience(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(UserWorkExperience userWorkExperience, int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userWorkExperience, Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(UserWorkExperience userWorkExperience, int Id)
        {
            return $" UPDATE UserWorkExperiences " +
                   $" SET AccountId = {userWorkExperience.AccountId}," +
                   $" UserId = {userWorkExperience.UserId}," +
                   $" WorkTitle = '{userWorkExperience.WorkTitle}'," +
                   $" WorkDescription = '{userWorkExperience.WorkDescription}'," +
                   $" WorkStartDate = '{userWorkExperience.WorkStartDate}'," +
                   $" WorkEndDate = '{userWorkExperience.WorkEndDate}'" +
                   $" WHERE Id = {Id}";
        }
    }
}