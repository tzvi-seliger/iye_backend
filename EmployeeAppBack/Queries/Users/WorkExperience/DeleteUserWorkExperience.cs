﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.WorkExperience
{
    public class DeleteUserWorkExperience
    {
        private readonly string connectionString;

        public DeleteUserWorkExperience(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(int Id)
        {
            return $"Delete from [dbo].[UserWorkExperiences] WHERE Id = {Id}";
        }
    }
}