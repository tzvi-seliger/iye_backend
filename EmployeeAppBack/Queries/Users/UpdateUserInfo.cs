﻿using EmployeeAppBack.Models;
using EmployeeAppBack.Services;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class UpdateUserInfo
    {
        private readonly string connectionString;

        public UpdateUserInfo(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(User user, int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(user, Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(User user, int Id)
        {
            return  
                $"UPDATE[dbo].[Users] " +
                $"SET[AccountID] = '{user.AccountId}', " +
                $"[UserType] = {user.UserType}', " +
                $"[UserName] = {user.UserName}'," +
                $"[PasswordString] = {user.PasswordString}', " +
                $"[Salt] = {user.Salt}', " +
                $"[UserEmailAddress] = {user.UserEmailAddress}', " +
                $"[UserFirstName] = {user.UserFirstName}', " +
                $"[UserLastName] = {user.UserLastName}', " +
                $"[UserPhoneNumber] = {user.UserPhoneNumber}', " +
                $"[ManagerId] = {user.ManagerUserId}', " +
                $"[ProfilePhotoFilepath] = {user.ProfilePhotoFilepath}', " +
                $"WHERE Id = {Id}";
        }
    }
}