﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Trainings
{
    public class GetUserAuthoredTrainings
    {
        private readonly string connectionString;

        public GetUserAuthoredTrainings(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Training> Execute(int userId)
        {
            List<Training> authoredTrainings = new List<Training>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    authoredTrainings.Add(new Training
                    {
                        AccountID = Convert.ToInt32(reader["AccountId"]),
                        TrainingID = Convert.ToInt32(reader["UserId"]),
                        TrainingName = Convert.ToString(reader["TrainingName"]),
                        TrainingDescription = Convert.ToString(reader["TrainingDescription"])
                    });
                }
            }
            return authoredTrainings;
        }

        public string BuildQuery(int userId)
        {
            return $@"SELECT * FROM [db982997332].[dbo].[Trainings] where UserId = {userId};";
        }
    }
}