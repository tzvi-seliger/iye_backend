﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Trainings
{
    public class AddUserTraining
    {
        private readonly string connectionString;

        public AddUserTraining(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(PostUserTraining employeeTraining)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(employeeTraining), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(PostUserTraining employeeTraining)
        {
            return $"insert into UserTrainings(UserID, TrainingID, TrainingStatus) values({employeeTraining.UserId}, {employeeTraining.TrainingId}, '{employeeTraining.TrainingStatus}')";
        }
    }
}