﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Trainings
{
    public class GetEmployeeTrainingsById
    {
        private readonly string connectionString;

        public GetEmployeeTrainingsById(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<EmployeeTraining> Execute(int userId)
        {
            List<EmployeeTraining> trainings = new List<EmployeeTraining>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new EmployeeTraining
                    {
                        TrainingId = Convert.ToInt32(reader["TrainingId"]),
                        TrainingName = Convert.ToString(reader["TrainingName"]),
                        TrainingStatus = Convert.ToString(reader["TrainingStatus"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                    });
                }
            }
            return trainings;
        }

        public string BuildQuery(int userId)
        {
            return $@"select
  a.trainingid,
  b.trainingname,
  a.trainingstatus,
  c.userfirstname,
  c.userlastname
  from usertrainings a
  join trainings b on a.TrainingID = b.TrainingID
  join users c on a.UserID = c.UserID
  where c.UserId = '{userId}'";
        }
    }
}