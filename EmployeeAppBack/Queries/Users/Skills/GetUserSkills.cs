﻿using EmployeeAppBack.HelperMethods;
using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Queries.Users.Skills
{
    public class GetUserSkills
    {
        private readonly string connectionString;

        public GetUserSkills(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<UserSkill> Execute(int Id)
        {
            List<UserSkill> userSkills = new List<UserSkill>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    userSkills.Add(new UserSkill()
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        UserId = Convert.ToInt32(reader["UserId"]),
                        Skill = Convert.ToString(reader["Skill"]),
                        Description = Convert.ToString(reader["Description"])

                    });
                }
            }
            return userSkills;
        }

        public string BuildQuery(int Id)
        {
            return $"SELECT a.[Id], a.[UserID] ,a.Skill ,a.[Description] FROM [db982997332].[dbo].[UserSkills] a join users b on a.UserID = b.UserID where b.UserID = {Id}";
        }
    }
}
