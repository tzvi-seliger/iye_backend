﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Skills
{
    public class AddUserSkill
    {
        private readonly string connectionString;

        public AddUserSkill(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(UserSkill userSkill)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userSkill), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(UserSkill userSkill)
        {
            return $"insert into UserSkills (UserId, Skill, Description) values(" +
                $" {userSkill.UserId}," +
                $"'{userSkill.Skill}'," +
                $"'{userSkill.Description}')";
        }
    }
}