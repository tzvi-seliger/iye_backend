﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Skills
{
    public class DeleteUserSkill
    {
        private readonly string connectionString;

        public DeleteUserSkill(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(int Id)
        {
            return $"Delete from [dbo].[UserSkills] WHERE Id = {Id}";
        }
    }
}