﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Skills
{
    public class UpdateUserSkill
    {
        private readonly string connectionString;

        public UpdateUserSkill(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(UserSkill userSkill, int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userSkill, Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(UserSkill userSkill, int Id)
        {
            return $"UPDATE UserSkills SET Skill = '{userSkill.Skill}', Description = '{userSkill.Description}'  WHERE Id = {Id}";
        }
    }
}