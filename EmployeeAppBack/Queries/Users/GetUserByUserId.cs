﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class GetUserByUserId
    {
        public readonly string connectionString;

        public GetUserByUserId(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<User> Execute(int userid)
        {
            List<User> users = new List<User>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userid), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User
                    {
                        AccountId = Convert.ToInt32(reader["AccountId"]),
                        UserId = Convert.ToInt32(reader["UserId"]),
                        PasswordString = Convert.ToString(reader["PasswordString"]),
                        UserName = Convert.ToString(reader["UserName"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                        Salt = Convert.ToString(reader["Salt"]),
                        UserEmailAddress = Convert.ToString(reader["UserEmailAddress"]),
                        UserPhoneNumber = Convert.ToString(reader["UserPhoneNumber"]),
                        UserType = Convert.ToString(reader["UserType"]),
                        ProfilePhotoFilepath = Convert.ToString(reader["ProfilePhotoFilepath"])
                    });
                }
            }
            return users;
        }

        public string BuildQuery(int userid)
        {
            return $"SELECT * from USERS WHERE UserId = {userid}";
        }
    }
}