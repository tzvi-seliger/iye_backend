﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class GetEmployeeTrainingsByUserId
    {
        private readonly string connectionString;

        public GetEmployeeTrainingsByUserId(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<EmployeeTraining> Execute(string userName)
        {
            List<EmployeeTraining> trainings = new List<EmployeeTraining>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userName), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new EmployeeTraining
                    {
                        TrainingName = Convert.ToString(reader["TrainingName"]),
                        TrainingStatus = Convert.ToString(reader["TrainingStatus"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                    });
                }
            }
            return trainings;
        }

        public string BuildQuery(string userName)
        {
            return $@"select
  b.trainingname,
  a.trainingstatus,
  c.userfirstname,
  c.userlastname
  from usertrainings a
  join trainings b on a.TrainingID = b.TrainingID
  join users c on a.UserID = c.UserID
  where c.UserName = '{userName}'";
        }
    }
}