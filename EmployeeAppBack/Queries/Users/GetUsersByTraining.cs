﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class GetUsersByTraining
    {
        private readonly string connectionString;

        public GetUsersByTraining(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<BasicUser> Execute(int trainingId)
        {
            List<BasicUser> users = new List<BasicUser>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(trainingId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new BasicUser
                    {
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"])
                    });
                }
            }
            return users;
        }

        public string BuildQuery(int trainingId)
        {
            return $@"select
  userfirstname,
  userlastname
  from users
  where userid in (select userid from UserTrainings where trainingid = {trainingId})";
        }
    }
}