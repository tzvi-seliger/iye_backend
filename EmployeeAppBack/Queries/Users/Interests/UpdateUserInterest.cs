﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Interests
{
    public class UpdateUserInterest
    {
        private readonly string connectionString;

        public UpdateUserInterest(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(UserInterest userInterest, int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userInterest, Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(UserInterest userInterest, int Id)
        {
            return $"UPDATE UserInterests SET Interest = '{userInterest.Interest}', Description = '{userInterest.Description}'  WHERE Id = {Id}";
        }
    }
}