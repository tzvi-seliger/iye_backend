﻿using EmployeeAppBack.Models;
using Remotion.Linq.Clauses.ResultOperators;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Interests
{
    public class DeleteUserInterest
    {
        private readonly string connectionString;

        public DeleteUserInterest(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(int Id)
        {
            var sql = $"Delete from [db982997332].[dbo].[UserInterests] WHERE Id = {Id}";
            return  sql;
        }
    }
}