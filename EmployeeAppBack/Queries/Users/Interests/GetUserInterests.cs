﻿using EmployeeAppBack.HelperMethods;
using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAppBack.Queries.Users.Interests
{
    public class GetUserInterests
    {
        private readonly string connectionString;

        public GetUserInterests(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<GetUserInterest> Execute(int Id)
        {
            List<GetUserInterest> userInterests = new List<GetUserInterest>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    userInterests.Add(new GetUserInterest()
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        UserId = Convert.ToInt32(reader["UserId"]),
                        Interest = Convert.ToString(reader["Interest"]),
                        Description = Convert.ToString(reader["Description"])

                    });
                }
            }
            return userInterests;
        }

        public string BuildQuery(int Id)
        {
            return $"SELECT a.Id, a.[UserID] ,a.Interest ,a.[Description] FROM [db982997332].[dbo].[UserInterests] a join users b on a.UserID = b.UserID where b.UserID = {Id}";
        }
    }
}
