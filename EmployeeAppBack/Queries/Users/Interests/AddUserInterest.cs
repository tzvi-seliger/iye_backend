﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users.Interests
{
    public class AddUserInterest
    {
        private readonly string connectionString;

        public AddUserInterest(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(UserInterest userInterest)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userInterest), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(UserInterest userInterest)
        {
            return $"insert into UserInterests(UserId, Interest, Description) values(" +
                $"'{userInterest.UserId}'," +
                $"'{userInterest.Interest}'," +
                $"'{userInterest.Description}')";
        }
    }
}