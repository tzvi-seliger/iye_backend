﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class GetUsers
    {
        private readonly string connectionString;

        public GetUsers(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<User> Execute()
        {
            List<User> users = new List<User>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User
                    {
                        UserId = Convert.ToInt32(reader["UserId"]),
                        AccountId = Convert.ToInt32(reader["AccountID"]),
                        UserType = Convert.ToString(reader["UserType"]),
                        UserName = Convert.ToString(reader["UserName"]),
                        PasswordString = Convert.ToString(reader["PasswordString"]),
                        Salt = Convert.ToString(reader["Salt"]),
                        UserEmailAddress = Convert.ToString(reader["UserEmailAddress"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                        UserPhoneNumber = Convert.ToString(reader["UserPhoneNumber"]),
                        ProfilePhotoFilepath = Convert.ToString(reader["ProfilePhotoFilepath"]),
                    });
                }
            }
            return users;
        }

        public string BuildQuery()
        {
            return "SELECT * FROM Users";
        }
    }
}