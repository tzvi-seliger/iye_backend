﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Users
{
    public class GetUserByUserNameAndPassword
    {
        public readonly string connectionString;

        public GetUserByUserNameAndPassword(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<User> Execute(string userName, string password)
        {
            List<User> users = new List<User>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userName, password), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    users.Add(new User
                    {
                        AccountId = Convert.ToInt32(reader["AccountId"]),
                        UserId = Convert.ToInt32(reader["UserId"]),
                        PasswordString = Convert.ToString(reader["PasswordString"]),
                        UserName = Convert.ToString(reader["UserName"]),
                        UserFirstName = Convert.ToString(reader["UserFirstName"]),
                        UserLastName = Convert.ToString(reader["UserLastName"]),
                        Salt = Convert.ToString(reader["Salt"]),
                        UserEmailAddress = Convert.ToString(reader["UserEmailAddress"]),
                        UserPhoneNumber = Convert.ToString(reader["UserPhoneNumber"]),
                        UserType = Convert.ToString(reader["UserType"]),
                        ProfilePhotoFilepath = Convert.ToString(reader["UserType"])
                    });
                }
            }
            return users;
        }

        public string BuildQuery(string userName, string password)
        {
            return $"SELECT * from USERS WHERE UserName = '{userName}' AND PasswordString = '{password}'";
        }
    }
}