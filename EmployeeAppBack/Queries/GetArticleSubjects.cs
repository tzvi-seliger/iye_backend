﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetArticleSubjects
    {
        private readonly string connectionString;

        public GetArticleSubjects(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<string> Execute()
        {
            List<string> subjects = new List<string>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    subjects.Add(Convert.ToString(reader["Subject"]));
                }
            }
            return subjects;
        }

        public string BuildQuery()
        {
            return $"SELECT DISTINCT Subject FROM Articles";
        }
    }
}