﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetArticleRatings
    {
        private readonly string connectionString;

        public GetArticleRatings(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<int> Execute()
        {
            List<int> ratings = new List<int>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    ratings.Add(Convert.ToInt32(reader["Rating"]));
                }
            }
            return ratings;
        }

        public string BuildQuery()
        {
            return $"SELECT DISTINCT Rating FROM Articles";
        }
    }
}