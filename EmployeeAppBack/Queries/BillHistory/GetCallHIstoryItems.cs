﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.BillHistory
{
    public class GetCallHistoryItems
    {
        private readonly string connectionString;

        public GetCallHistoryItems(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<BillHistoryEntry> Execute(string id)
        {
            List<BillHistoryEntry> positions = new List<BillHistoryEntry>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open(); 
                 SqlCommand SelectAll = new SqlCommand(BuildQuery(id), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    positions.Add(new BillHistoryEntry
                    {
                        UserId = Convert.ToInt32(reader["UserId"]),
                        CallingPartyId = Convert.ToInt32(reader["CallingPartyId"]),
                        CallTimeStamp = Convert.ToDateTime(reader["CallTimeStamp"]),
                        RateAtTimeOfCall = Convert.ToInt32(reader["RateAtTimeOfCall"]),
                    });
                }
            }
            return positions;
        }

        public string BuildQuery(string id)
        {
            return $" [UserId]" +
                $" ,[CallingPartyId]" +
                $" ,[CallTimeStamp]" +
                $" ,[RateAtTimeOfCall]" +
                $" FROM[Employee_Management].[dbo].[CallHistoryItems] WHERE UserId = {id}";
        }
    }
}