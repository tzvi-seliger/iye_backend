﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.BillHistory
{
    public class EditCallHistoryItem
    {
        private readonly string connectionString;

        public EditCallHistoryItem(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(BillHistoryEntry billHistoryEntry)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(billHistoryEntry), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(BillHistoryEntry billHistoryEntry)
        {
            return $"UPDATE [dbo].[CallHistoryItems] " +
                $"SET [UserId] = UserId" +
                $",[CallingPartyId] = @CallingPartyId " +
                $",[CallTimeStamp] = @CallTimeStamp " +
                $",[RateAtTimeOfCall] = @RateAtTimeOfCall " +
                $"WHERE [UserId] = @UserId";
        }
    }
}