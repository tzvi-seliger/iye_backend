﻿using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.BillHistory
{
    public class DeleteCallHistoryItem
    {
        private readonly string connectionString;

        public DeleteCallHistoryItem(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(string id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(string id)
        {
            return $"Delete from CallHistoryItems WHERE UserId = {id}";
        }
    }
}