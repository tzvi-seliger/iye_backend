﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.BillHistory
{
    public class GetBillTotal
    {
        private readonly string connectionString;

        public GetBillTotal(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int Execute(string Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                var totalRow = (int)SelectAll.ExecuteScalar();
                return totalRow;
            }
        }

        public string BuildQuery(string Id)
        {
            return $"SELECT SUM((DATEDIFF(MINUTE,[CallTimeStamp], [EndCallTimeStamp])) * ([RateAtTimeOfCall] / 60.0)) AS TOTAL " +
                $"FROM[dbo].[CallHistoryItems] " +
                $"WHERE userId = {Id}";

        }
    }
}