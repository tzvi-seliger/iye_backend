﻿using EmployeeAppBack.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.BillHistory
{
    public class AddCallHistoryItem
    {
        private string connectionString;

        public AddCallHistoryItem(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(BillHistoryEntry callHistoryItem)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand Add = new SqlCommand(BuildQuery(out List<SqlParameter> parameters, callHistoryItem), conn);
                Add.Parameters.AddRange(parameters.ToArray());
                Add.ExecuteNonQuery();

            }
        }

        public string BuildQuery(out List<SqlParameter> parameters, BillHistoryEntry billEntry)
        {
            parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter
            {
                ParameterName = "@UserId",
                Value = billEntry.UserId,
                SqlDbType = SqlDbType.Int
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = "@CallingPartyId",
                Value = billEntry.CallingPartyId,
                SqlDbType = SqlDbType.Int
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = "@CallTimeStamp",
                Value = billEntry.CallTimeStamp,
                SqlDbType = SqlDbType.DateTime
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = "@RateAtTimeOfCall",
                Value = billEntry.RateAtTimeOfCall,
                SqlDbType = SqlDbType.Int
            });


            return $"INSERT INTO[dbo].[CallHistoryItems]" +
                $"([UserId]" +
                $",[CallingPartyId]" +
                $",[CallTimeStamp]" +
                $",[RateAtTimeOfCall])" +
                $"VALUES" +
                $"(@UserId" +
                $",@CallingPartyId" +
                $",@CallTimeStamp" +
                $",@RateAtTimeOfCall)" +
                $"GO";
        }
    }
}