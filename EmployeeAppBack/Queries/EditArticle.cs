﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class EditArticle
    {
        private readonly string connectionString;

        public EditArticle(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Article article)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(article), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Article article)
        {

            var query = $"UPDATE Articles SET Title='{article.Title}', Description='{article.Description}', Content='{article.Content}', Author='{article.Author}',  Level='{article.Level}',  Subject='{article.Subject}',  Rating={article.Rating} WHERE Id = {article.Id}";

            return query; 
        }
    }
}