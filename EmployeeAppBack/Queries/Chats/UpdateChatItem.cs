﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Chats
{
    public class UpdateChatItem
    {
        private readonly string connectionString;

        public UpdateChatItem(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(string chat, int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(chat, Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(string chat, int Id)
        {
            return $"UPDATE Chats SET Text = '{chat}' WHERE Id = {Id}";
        }
    }
}