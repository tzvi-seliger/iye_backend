﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Chats
{
    public class AddChat
    {
        private readonly string connectionString;

        public AddChat(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Chat chat)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(chat), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Chat chat)
        {
            return $"insert into [db982997332].[dbo].Chats(SenderUserId, RecipientUserId, Text) values(" +
                $" {chat.SenderUserId}," +
                $" {chat.RecipientUserId}," +
                $"'{chat.Text}')";
        }
    }
}