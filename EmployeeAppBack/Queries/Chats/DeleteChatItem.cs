﻿
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Chats
{
    public class DeleteChatItem
    {
        private readonly string connectionString;

        public DeleteChatItem(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(int Id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(Id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(int Id)
        {
            var sql = $"Delete from [db982997332].[dbo].[Chats] WHERE Id = {Id}";
            return  sql;
        }
    }
}

