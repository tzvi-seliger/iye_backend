﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Chats
{
    public class GetAllUserToUserChats
    {
        private readonly string connectionString;

        public GetAllUserToUserChats(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Chat> Execute(int senderId, int recipientId)
        {
            List<Chat> chats = new List<Chat>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(senderId, recipientId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    chats.Add(new Chat()
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        SenderUserId = Convert.ToInt32(reader["SenderUserId"]),
                        RecipientUserId = Convert.ToInt32(reader["RecipientUserId"]),
                        Text = Convert.ToString(reader["Text"])

                    });
                }
            }
            return chats;
        }

        public string BuildQuery(int senderId, int recipientId)
        {
            return $"select * from [db982997332].[dbo].Chats where senderUserId = {senderId} and RecipientUserId = {recipientId} or  senderUserId = {recipientId} and RecipientUserId = {senderId}";
        }
    }
}
