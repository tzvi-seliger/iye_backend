﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class AddArticle
    {
        private readonly string connectionString;

        public AddArticle(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Article article)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(article), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Article article)
        {
            return $"insert into Articles(Title, Description, Content, Author, Level, Subject, Rating) values('{article.Title}', '{article.Description}', '{article.Content}',  '{article.Author}',  '{article.Level}',  '{article.Subject}',  '{article.Rating}')";
        }
    }
}