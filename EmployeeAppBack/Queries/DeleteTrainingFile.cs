﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class DeleteTrainingFile : IDBQuery
    {
        private readonly int _trainingId;
        private readonly int _orderNo;

        public DeleteTrainingFile(int trainingId, int orderNo)
        {
            _trainingId = trainingId;
            _orderNo = orderNo;
        }

        public void Execute(SqlConnection conn)
        {
            SqlCommand Add = new SqlCommand(BuildQuery(out List<SqlParameter> parameters), conn);
            Add.Parameters.AddRange(parameters.ToArray());
            Add.ExecuteNonQuery();
        }

        public string BuildQuery(out List<SqlParameter> parameters)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter
            {
                ParameterName = "@TrainingId",
                Value = _trainingId,
                SqlDbType = SqlDbType.Int
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = "@OrderNo",
                Value = _orderNo,
                SqlDbType = SqlDbType.Int
            });

            return @"
  delete from [dbo].[TrainingFiles]
  where TrainingId = @TrainingId
  AND OrderNo = @OrderNo";
        }
    }
}