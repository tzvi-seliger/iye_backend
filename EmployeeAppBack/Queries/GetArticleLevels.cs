﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetArticleLevels
    {
        private readonly string connectionString;

        public GetArticleLevels(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<string> Execute()
        {
            List<string> levels = new List<string>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    levels.Add(Convert.ToString(reader["Level"]));
                }
            }
            return levels;
        }

        public string BuildQuery()
        {
            return $"SELECT DISTINCT Level FROM Articles";
        }
    }
}