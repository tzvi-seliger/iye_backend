﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Definitions
{
        public class AddTermWithDefinition
    {
            private readonly string connectionString;

            public AddTermWithDefinition(string connectionString)
            {
                this.connectionString = connectionString;
            }

            public void Execute(TermDefinition termDefinition)
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand SelectAll = new SqlCommand(BuildQuery(termDefinition), conn);
                    SelectAll.ExecuteNonQuery();
                }
            }

            public string BuildQuery(TermDefinition termDefinition)
            {
                return $"USE [TorahQuestions]; " +
                $"INSERT INTO[dbo].[Definitions] ([Term] ,[Definition]) VALUES (N'{ termDefinition.Term }', N'{ termDefinition.Definition }');";
            }
        }
    }