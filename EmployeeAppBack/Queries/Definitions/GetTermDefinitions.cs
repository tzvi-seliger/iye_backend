﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Definitions
{
    public class GetTermDefinitions
    {
        private readonly string connectionString;

        public GetTermDefinitions(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<TermDefinition> Execute()
        {
            List<TermDefinition> termDefinitions = new List<TermDefinition>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    termDefinitions.Add(new TermDefinition
                    {
                        Definition = Convert.ToString(reader["Definition"]),
                        Term = Convert.ToString(reader["Term"])
                    });
                }
            }

            return termDefinitions;
        }

        public string BuildQuery()
        {
            return $"SELECT[Term], [Definition] FROM [TorahQuestions].[dbo].[Definitions]";
        }
    }
}