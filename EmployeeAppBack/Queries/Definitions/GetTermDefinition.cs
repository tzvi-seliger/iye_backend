﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Definitions
{
    public class GetTermDefinition
    {
        private readonly string connectionString;

        public GetTermDefinition(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<TermDefinition> Execute(string term)
        {
            List<TermDefinition> termDefinitions = new List<TermDefinition>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(term), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    termDefinitions.Add(new TermDefinition
                    {
                        Definition = Convert.ToString(reader["Definition"]),
                        Term = Convert.ToString(reader["Term"])
                    });
                }
            }
        

            return termDefinitions;
        }


        public string BuildQuery(string term)
        {
            return $"SELECT [Term], [Definition] FROM [TorahQuestions].[dbo].[Definitions] where TERM = N'{term}' ";
        }
    }
}
