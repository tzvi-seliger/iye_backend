﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Definitions
{
    public class UpdateTermDefinition
    {
        private readonly string connectionString;

        public UpdateTermDefinition(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(TermDefinition termDefinition)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(termDefinition), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(TermDefinition termDefinition)
        {
            return $"USE [TorahQuestions];" +
                $"UPDATE [dbo].[Definitions] SET [Definition] = N'{ termDefinition.Definition }' WHERE TERM = N'{ termDefinition.Term }';";
        }
    }
}



