﻿
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Trainings
{
    public class GetLastTrainingNumber
    {
        private readonly string connectionString;

        public GetLastTrainingNumber(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int Execute(int trainingId)
        {
            int orderNo;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(trainingId), conn);
                orderNo = (int)SelectAll.ExecuteScalar();
            }
            return orderNo;
        }

        public string BuildQuery(int trainingId)
        {
            return $"select top 1 orderno from Trainingfiles where trainingid = {trainingId} order by orderno asc";
        }
    }
}