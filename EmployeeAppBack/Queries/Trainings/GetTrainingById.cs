﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries.Trainings
{
    public class GetTrainingById
    {
        private readonly string connectionString;

        public GetTrainingById(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Training Execute(int trainingId)
        {
           List<Training> trainings = new List<Training>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(trainingId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    trainings.Add(new Training
                    {
                        AccountID = Convert.ToInt32(reader["AccountID"]),
                        TrainingDescription = Convert.ToString(reader["TrainingDescription"]),
                        TrainingID = Convert.ToInt32(reader["TrainingID"]),
                        TrainingName = Convert.ToString(reader["TrainingName"]),
                    });
                }
            }
            return trainings.FirstOrDefault();
        }

        public string BuildQuery(int trainingId)
        {
            return $"SELECT * FROM trainings where TrainingId = {trainingId}";
        }
    }
}