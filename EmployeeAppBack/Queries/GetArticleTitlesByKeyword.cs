﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetArticleTitlesByKeyword
    {
        private readonly string connectionString;

        public GetArticleTitlesByKeyword(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<ArticleBase> Execute(string keyword)
        {
            List<ArticleBase> articles = new List<ArticleBase>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(keyword), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    articles.Add(new ArticleBase
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        Title = Convert.ToString(reader["Title"]),
                        Description = Convert.ToString(reader["Description"])
                    });
                }
            }
            return articles;
        }

        public string BuildQuery(string keyword)
        {
            return $"SELECT Id, Title, Description FROM Articles WHERE Title Like '%{keyword}%'";
        }
    }
}