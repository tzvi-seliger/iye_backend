﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class DeleteArticle
    {
        private readonly string connectionString;

        public DeleteArticle(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(string id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(string id)
        {
            return $"Delete from Articles WHERE Id = {id}";
        }
    }
}