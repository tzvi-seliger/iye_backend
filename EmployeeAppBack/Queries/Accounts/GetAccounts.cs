﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries.Accounts
{
    public class GetAccounts
    {
        private readonly string connectionString;

        public GetAccounts(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Account> Execute()
        {
            List<Account> accounts = new List<Account>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    accounts.Add(
                        new Account()
                        {
                            AccountDescription = Convert.ToString(reader["AccountDescription"]),
                            AccountId = Convert.ToInt32(reader["AccountId"]),
                            AccountLogo = Convert.ToString(reader["AccountLogo"]),
                            AccountName = Convert.ToString(reader["AccountName"]),
                        });
                }
            }
            return accounts;
        }

        public string BuildQuery()
        {
            return $"SELECT * FROM [db982997332].[dbo].[Accounts]";
        }
    }
}