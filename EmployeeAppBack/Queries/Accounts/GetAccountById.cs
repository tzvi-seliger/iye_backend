﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries.Accounts
{
    public class GetAccountById
    {
        private readonly string connectionString;

        public GetAccountById(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Account Execute(int accountId)
        {
            List<Account> accounts = new List<Account>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(accountId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    accounts.Add(
                        new Account() {
                            AccountDescription = Convert.ToString(reader["AccountDescription"]),
                            AccountId = Convert.ToInt32(reader["AccountId"]),
                            AccountLogo = Convert.ToString(reader["AccountLogo"]),
                            AccountName = Convert.ToString(reader["AccountName"]),
                    });
                }
            }
            return accounts.FirstOrDefault();
        }

        public string BuildQuery(int accountId)
        {
            return $"SELECT * FROM [db982997332].[dbo].[Accounts] WHERE AccountId = {accountId}";
        }
    }
}