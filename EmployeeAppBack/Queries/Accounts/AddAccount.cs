﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries.Accounts
{
    public class AddAccount
    {
        private readonly string connectionString;

        public AddAccount(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Account account)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(account), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Account account)
        {
            return $"INSERT INTO [dbo].[Accounts] ([AccountName], [AccountDescription], [AccountLogo]) VALUES ('{account.AccountName}', '{account.AccountDescription}', '{account.AccountLogo}');";
        }
    }
}