﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries.Accounts
{
    public class DeleteAccountById
    {
        private readonly string connectionString;

        public DeleteAccountById(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(int accountId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(accountId), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(int accountId)
        {
            return $"Delete FROM [dbo].[Accounts] WHERE AccountId = {accountId} ;";
        }
    }
}