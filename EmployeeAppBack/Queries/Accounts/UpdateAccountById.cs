﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace EmployeeAppBack.Queries.Accounts
{
    public class UpdateAccountById
    {
        private readonly string connectionString;

        public UpdateAccountById(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Account account)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(account), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Account account)
        {
            return $"UPDATE [dbo].[Accounts] SET [AccountName] = account.AccountName ,[AccountDescription] = {account.AccountDescription} ,[AccountLogo] ={ account.AccountLogo} WHERE AccountId = {account.AccountId } ";
        }
    }
}