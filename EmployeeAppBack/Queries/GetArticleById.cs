﻿using EmployeeAppBack.Models;
using System.Collections.Generic;
using EmployeeAppBack.HelperMethods;

namespace EmployeeAppBack.Queries
{
    // get each prop in class make it into a table 
    public class GetArticleById
    {
        private readonly string connectionString;

        public GetArticleById(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public Article Execute(int Id)
        {
            return QueryBuilder<Article>.ExecuteGeneric(BuildQuery(Id), connectionString);
        }

        public string BuildQuery(int Id)
        {
            return QueryBuilder<Article>.Select(new List<string> { }, "Id", Id.ToString(), "int", "Articles");
        }
    }
}