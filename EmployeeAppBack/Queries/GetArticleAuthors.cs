﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetArticleAuthors
    {
        private readonly string connectionString;

        public GetArticleAuthors(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<string> Execute()
        {
            List<string> authors = new List<string>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    authors.Add(Convert.ToString(reader["Author"]));
                }
            }
            return authors;
        }

        public string BuildQuery()
        {
            return $"SELECT DISTINCT Author FROM Articles";
        }
    }
}