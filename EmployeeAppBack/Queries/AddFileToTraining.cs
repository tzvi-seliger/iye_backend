﻿using EmployeeAppBack.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class AddFileToTraining : IDBQuery
    {
        private TrainingFileInsertion _insertion;

        public AddFileToTraining(TrainingFileInsertion insertion)
        {
            _insertion = insertion;
        }

        public void Execute(SqlConnection conn)
        {
            SqlCommand Add = new SqlCommand(BuildQuery(out List<SqlParameter> parameters), conn);
            Add.Parameters.AddRange(parameters.ToArray());
            Add.ExecuteNonQuery();
        }

        public string BuildQuery(out List<SqlParameter> parameters)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter
            {
                ParameterName = "@TrainingId",
                Value = _insertion.TrainingId,
                SqlDbType = SqlDbType.Int
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = "@OrderNo",
                Value = _insertion.OrderNo,
                SqlDbType = SqlDbType.Int
            });
            parameters.Add(new SqlParameter
            {
                ParameterName = "@Description",
                Value = _insertion.Description,
                SqlDbType = SqlDbType.VarChar
            });

            parameters.Add(new SqlParameter
            {
                ParameterName = "@FilePath",
                Value = _insertion.FilePath,
                SqlDbType = SqlDbType.VarChar
            });

            parameters.Add(new SqlParameter
            {
                ParameterName = "@IsComposite",
                Value = _insertion.IsComposite,
                SqlDbType = SqlDbType.Bit
            });

            parameters.Add(new SqlParameter
            {
                ParameterName = "@TrainingIdReference",
                Value = _insertion.TrainingIdReference,
                SqlDbType = SqlDbType.Int
            });

            parameters.Add(new SqlParameter
            {
                ParameterName = "@OrderNoReference",
                Value = _insertion.OrderNoReference,
                SqlDbType = SqlDbType.Int
            });
            string query = "";
            if (_insertion.OrderNo == 0)
            {
                query = string.Format(@"
INSERT INTO [dbo].[TrainingFiles]
           ([TrainingId]
           ,[OrderNo]
           ,[Description]
           ,[FilePath]
           ,[IsComposite]
           ,[TrainingIdReference]
           ,[OrderNoReference])
     VALUES
           (@TrainingId
           ,{0}
           ,@Description
           ,@FilePath
           ,@IsComposite
           ,@TrainingIdReference
           ,@OrderNoReference)", "select(select top 1 orderno from trainingfiles where trainingId = @TrainingId order by OrderNo desc) + 1");
            }
            else
            {
                query = string.Format(@"
INSERT INTO [dbo].[TrainingFiles]
           ([TrainingId]
           ,[OrderNo]
           ,[Description]
           ,[FilePath]
           ,[IsComposite]
           ,[TrainingIdReference]
           ,[OrderNoReference])
     VALUES
           (@TrainingId
           ,{0}
           ,@Description
           ,@FilePath
           ,@IsComposite
           ,@TrainingIdReference
           ,@OrderNoReference)", "@OrderNo");
            }
            return query;
        }
    }
}