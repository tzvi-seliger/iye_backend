﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class PostMessage
    {
        private readonly string connectionString;

        public PostMessage(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Message message)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(message), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Message message)
        {
            return $"insert into Messages(SenderId, ReceiverId, Content) values({message.SenderId}, {message.ReceiverId}, '{message.Content}')";
        }
    }
}