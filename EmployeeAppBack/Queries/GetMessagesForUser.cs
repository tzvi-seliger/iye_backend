﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetMessagesForUser
    {
        private readonly string connectionString;

        public GetMessagesForUser(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Message> Execute(int userId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                List<Message> messages = new List<Message>();
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    messages.Add(new Message
                    {
                        Id = Convert.ToInt32(reader["Id"]),
                        SenderId = Convert.ToInt32(reader["SenderId"]),
                        ReceiverId = Convert.ToInt32(reader["ReceiverId"]),
                        Content = Convert.ToString(reader["Content"])
                    });
                }
                return messages;
            }
        }

        public string BuildQuery(int userId)
        {
            return $"SELECT (b.UserID where b.userid = a.receiverId) FROM Messages a join Users b WHERE ReceiverId = {userId}'";
        }
    }
}