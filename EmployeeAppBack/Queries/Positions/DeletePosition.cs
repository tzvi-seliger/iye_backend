﻿using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Positions
{
    public class DeletePosition
    {
        private readonly string connectionString;

        public DeletePosition(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(string id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(id), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(string id)
        {
            return $"Delete from Positions WHERE PositionId = {id}";
        }
    }
}