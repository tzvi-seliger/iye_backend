﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Positions
{
    public class AddPosition
    {
        private readonly string connectionString;

        public AddPosition(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Position position)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(position), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Position position)
        { 
            var remote = position.IsRemote ? 1 : 0;
            return $"insert into Positions (AccountID, Description, SalaryMin, SalaryMax, Location, Title, IsRemote, UserId, ExperienceRequired) values(" +
                $" {position.AccountID}," +
                $"'{position.Description}'," +
                $"{position.SalaryMin}," +
                $"{position.SalaryMax}," +
                $"'{position.Location}'," +
                $"'{position.Title}'," +
                $"{remote}," +
                $"{position.UserId}, " +
                $"{position.ExperienceRequired})";
        }
    }
}