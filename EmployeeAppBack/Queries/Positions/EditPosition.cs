﻿using EmployeeAppBack.Models;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Positions
{
    public class EditPosition
    {
        private readonly string connectionString;

        public EditPosition(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Execute(Position position)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(position), conn);
                SelectAll.ExecuteNonQuery();
            }
        }

        public string BuildQuery(Position position)
        {
            return $"UPDATE [dbo].[Positions] " +
                $"SET " +
                $" [SalaryMin] = {position.SalaryMin} " +
                $",[SalaryMax] = {position.SalaryMax} " +
                $",[Location] = {position.Location} " +
                $",[IsRemote] = {position.IsRemote} " +
                $",[ExperienceRequired] = {position.ExperienceRequired} " +
                $" WHERE [PositionId] = {position.PositionId}; ";
        }
    }
}