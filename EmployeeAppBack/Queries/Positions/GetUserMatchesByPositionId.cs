﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Positions
{
    public class GetUserMatchesByPositionId
    {
        private readonly string connectionString;

        public GetUserMatchesByPositionId(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<User> Execute()
        {
            List<User> matchingUsers = new List<User>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    matchingUsers.Add(new User
                    {
                        //AccountID = Convert.ToInt32(reader["AccountID"]),
                        //PositionId = Convert.ToInt32(reader["PositionId"]),
                        //IsRemote = Convert.ToByte(reader["IsRemote"]),
                        //ExperienceRequired = Convert.ToInt32(reader["ExperienceRequired"]),
                        //Location = Convert.ToString(reader["Location"]),
                        //SalaryMax = Convert.ToInt32(reader["SalaryMax"]),
                        //SalaryMin = Convert.ToInt32(reader["SalaryMin"])
                    });
                }
            }
            return matchingUsers;
        }

        public string BuildQuery()
        {
            return $"SELECT[PositionId]" +
           $",[AccountID]" +
           $",[SalaryMin]" +
           $",[SalaryMax]" +
           $",[Location]" +
           $",[IsRemote]" +
           $",[ExperienceRequired]" +
           $"FROM[dbo].[Positions];";
        }
    }
}