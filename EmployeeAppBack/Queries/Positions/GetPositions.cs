﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries.Positions
{
    public class GetPositions
    {
        private readonly string connectionString;

        public GetPositions(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Position> Execute(int userId)
        {
            List<Position> positions = new List<Position>();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand SelectAll = new SqlCommand(BuildQuery(userId), conn);
                SqlDataReader reader = SelectAll.ExecuteReader();
                while (reader.Read())
                {
                    positions.Add(new Position
                    {
                        AccountID = Convert.ToInt32(reader["AccountID"]),
                        PositionId = Convert.ToInt32(reader["PositionId"]),
                        IsRemote = Convert.ToBoolean(reader["IsRemote"]),
                        ExperienceRequired = Convert.ToInt32(reader["ExperienceRequired"]),
                        Location = Convert.ToString(reader["Location"]),
                        SalaryMax = Convert.ToInt32(reader["SalaryMax"]),
                        SalaryMin = Convert.ToInt32(reader["SalaryMin"]),
                        Title = Convert.ToString(reader["Title"]),
                        Description = Convert.ToString(reader["Description"]),

                    });
                }
            }
            return positions;
        }

        public string BuildQuery(int userId)
        {
            return $"SELECT [PositionId]" +
           $",a.[AccountID]" +
           $",[SalaryMin]" +
           $",[SalaryMax]" +
           $",[Location]" +
           $",[IsRemote]" +
           $",[ExperienceRequired]" +
           $",[Title]" +
           $",[Description]" +
           $"FROM [dbo].[Users] a join [dbo].positions b on a.UserId = b.UserId where b.userId = {userId};";
        }
    }
}