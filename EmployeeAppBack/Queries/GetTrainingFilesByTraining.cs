﻿using EmployeeAppBack.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace EmployeeAppBack.Queries
{
    public class GetTrainingFilesByTraining : IDBQueryReturn
    {
        private readonly int _trainingId;
        private readonly List<TrainingFile> trainingFiles = new List<TrainingFile>();

        public GetTrainingFilesByTraining(int trainingId)
        {
            _trainingId = trainingId;
        }

        public List<TrainingFile> Execute(SqlConnection conn)
        {
            SqlCommand Add = new SqlCommand(BuildQuery(out List<SqlParameter> parameters), conn);
            Add.Parameters.AddRange(parameters.ToArray());
            SqlDataReader reader = Add.ExecuteReader();
            while (reader.Read())
            {
                trainingFiles.Add(new TrainingFile
                {
                    TrainingId = Convert.ToInt32(reader["TrainingId"]),
                    OrderNo = Convert.ToInt32(reader["OrderNo"]),
                    Description = Convert.ToString(reader["Description"]),
                    FilePath = Convert.ToString(reader["FilePath"]),
                    IsComposite = Convert.ToBoolean(reader["IsComposite"]),
                    TrainingIdReference = Convert.ToInt32(reader["TrainingIdReference"]),
                    OrderNoReference = Convert.ToInt32(reader["OrderNoReference"]),
                    AccountName = Convert.ToString(reader["AccountName"]),
                    TrainingName = Convert.ToString(reader["TrainingName"])
                });
            }
            return trainingFiles;
        }

        public string BuildQuery(out List<SqlParameter> parameters)
        {
            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter
            {
                ParameterName = "@TrainingId",
                Value = _trainingId,
                SqlDbType = SqlDbType.Int
            });

            return @"
      select
       d.[TrainingName]
      ,c.[TrainingId]
      ,c.[OrderNo]
      ,c.[Description]
      ,c.[FilePath]
      ,c.[IsComposite]
      ,c.trainingid as [TrainingIdReference]
      ,c.orderno as [OrderNoReference]
      ,e.accountname
      from trainingfiles c
      inner join trainings d on c.TrainingId = d.TrainingID
      inner join accounts e on d.AccountID = e.AccountID
      where iscomposite = 0
      and c.TrainingId = @TrainingId

      union
	  --return where idref is not null and ordernoref is null all trainings for id ref
      select
       d.[TrainingName]
      ,a.[TrainingId]
      ,a.[OrderNo]
      ,a.[Description]
      ,b.[FilePath]
      ,b.[IsComposite]
      ,b.trainingid as [TrainingIdReference]
      ,b.orderno as [OrderNoReference]
      ,e.accountname

      from trainingfiles a
      join trainingfiles b on a.trainingIdReference = b.trainingId
      inner join trainings d on a.TrainingId = d.TrainingID
      inner join accounts e on d.AccountID = e.AccountID

     where a.iscomposite = 1
     and a.ordernoreference is null
     and a.TrainingId = @TrainingId

     union
     select
       d.[TrainingName]
      ,a.[TrainingId]
      ,a.[OrderNo]
      ,a.[Description]
      ,b.[FilePath]
      ,a.[IsComposite]
      ,b.trainingid as [TrainingIdReference]
      ,b.orderno as [OrderNoReference]
      ,e.AccountName
      from trainingfiles a
      join trainingfiles b on a.trainingIdReference = b.trainingId
      inner join trainings d on a.TrainingId = d.TrainingID
      inner join accounts e on d.AccountID = e.AccountID
      where a.iscomposite = 1 and a.ordernoreference is not null and a.ordernoreference = b.orderno
      and a.TrainingId = @TrainingId";
        }
    }
}